import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/services/providers.dart';
import 'package:skripsi_project/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailProviderController extends GetxController with StateMixin<Map<String, dynamic>> {
  final storage = GetStorage();
  RxString latitudeProviderDetail = ''.obs;
  RxString longtitudeProviderDetail = ''.obs;
  RxString phoneNumberProvider = ''.obs;

  void getProviderDetail({String? search="", String? providerId}){
    Map<String, dynamic> data;
    change(null, status: RxStatus.loading());
    double latitude = storage.read("latitude") != null ? storage.read("latitude") : defaultLatitude;
    double longtitude = storage.read("longtitude") != null ? storage.read("longtitude") : defaultLongtitude;

    Providers()
    .getProvidersDetail(
      coordinate: [latitude, longtitude],
      providerId: providerId
    )
    .then((value) => {
      data = json.decode(value)['data']['data'],
      longtitudeProviderDetail.value = data['location']['provider_longitute'],
      latitudeProviderDetail.value = data['location']['provider_latitute'],
      phoneNumberProvider.value = data['provider_phone_number'],
      change(data, status: RxStatus.success()),    
    }).catchError((onError) {
      change(null, status: RxStatus.error());
    });
  }

  Future<void> openMap({
    String? latitude,
    String? longtitude
  }) async {
    Uri googleMapUrl = Uri.parse('https://www.google.com/maps/search/?api=1&query=$latitude,$longtitude');

    if(await canLaunchUrl(googleMapUrl)){
      await launchUrl(googleMapUrl, mode: LaunchMode.externalApplication);
    }else {
      throw 'Could not open the Map';
    }
  }

  Future<void> phoneCall(String url) async {
    Uri phoneUrl = Uri.parse(url);

    if(await canLaunchUrl(phoneUrl)){
      await launchUrl(phoneUrl, mode: LaunchMode.externalApplication);
    }else {
      throw "Could not launch $phoneUrl";
    }
  }

  @override
  void onInit() {
    getProviderDetail(providerId: Get.arguments);
    super.onInit();
  }
}
