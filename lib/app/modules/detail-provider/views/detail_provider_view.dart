import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/providers/detail-provider-skeleton.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/detail_provider_controller.dart';

class DetailProviderView extends GetView<DetailProviderController> {
  const DetailProviderView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final themeController = Get.find<ThemeController>();

    return Scaffold(      
      bottomNavigationBar: controller.obx((state) => Obx(() => BottomAppBar(
        color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
        child: Container(
          width: size.width,
          height: 70,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
            border: Border(
              top: BorderSide(color: themeController.isDarkMode.value ? Colors.grey.shade900 : Colors.grey.shade300)
            )
          ),
          child: Row(
            children: [
              Expanded(
                child: OutlinedButton(
                  onPressed: () {
                    controller.openMap(
                      latitude: controller.latitudeProviderDetail.value,
                      longtitude: controller.longtitudeProviderDetail.value
                    );
                  }, 
                  style: OutlinedButton.styleFrom(
                    side: BorderSide(color: AppTheme.primaryColorDark),
                    foregroundColor: AppTheme.primaryColorDark
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.map, color: AppTheme.primaryColorDark),
                      SizedBox(width: 10),
                      Text("detail_provider_text5".tr, style: TextStyle(color: AppTheme.primaryColorDark),)
                    ],
                  )
                ),
              ),
              SizedBox(width: 15),
              Expanded(
                child: ElevatedButton(
                  onPressed: () {
                    controller.phoneCall('tel:${controller.phoneNumberProvider}');
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: AppTheme.primaryColorDark
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.phone),
                      SizedBox(width: 10),
                      Text("detail_provider_text6".tr)
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      )), onLoading: DetailProviderSkeleton()),
      body: SingleChildScrollView(
        child: controller.obx((state) => 
          Container(
            child: Column(
              children: [
                Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Container(
                      width: size.width,
                      height: 250,
                      color: Colors.white,
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: state!['provider_banner'] != null ? state['provider_banner'] : 'https://down-ph.img.susercontent.com/file/ceda67835f47cbee57d0d547297c3956_tn',
                        progressIndicatorBuilder: (context, url, downloadProgress) => Center(child: CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark, strokeWidth: 2,)),
                      ),
                    ),
                    Positioned(
                      top: 40,
                      left: 20,
                      child: GestureDetector(
                        onTap: () => Get.back(),
                        child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white
                          ),
                          child: Icon(Icons.arrow_back_ios_new, size: 15, color: Colors.black,),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: -50,
                      height: 100,
                      child: Container(
                        width: size.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            width: 100,
                            height: 100,
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            decoration: BoxDecoration(
                              borderRadius:BorderRadius.circular(100),
                              color: Colors.white,
                              border: Border.all(color: Colors.grey)
                            ),
                            child: ClipRRect(
                              borderRadius:BorderRadius.circular(100),
                              child: CachedNetworkImage(
                                imageUrl: state['provider_logo'] != null ? state['provider_logo'] : 'https://down-ph.img.susercontent.com/file/ceda67835f47cbee57d0d547297c3956_tn',
                                progressIndicatorBuilder: (context, url, downloadProgress) => Center(child: CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark, strokeWidth: 2,)),
                              ),
                            ),
                          ),
                        )
                      ),
                    )
                  ],
                ),
        
                Container(
                  width: size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  margin: EdgeInsets.only(top: 70),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        state['provider_name'],
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(height: 15),
                      Text(state['location']['location_gmaps']),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                color: AppTheme.primaryColorDark
                              )
                            ),
                            child: Text(state['provider_type'], textAlign: TextAlign.center, style: TextStyle(color: AppTheme.primaryColorDark),),
                          ),
                          SizedBox(width: 10),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                color: Colors.grey
                              )
                            ),
                            child: Row(
                              children: [
                                Icon(Icons.location_on, size: 15, color: Colors.grey),
                                Text("${state['location']['distance'].toString().split('.')[0]}km", style: TextStyle(color: Colors.grey),)
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
        
                Obx(() => Container(
                  width: size.width,
                  height: 10,
                  margin: EdgeInsets.symmetric(vertical: 30),
                  color: themeController.isDarkMode.value ? HexColor("#0e122f") : AppTheme.backgroundColor,
                )),
        
                Container(
                  width: size.width,
                  // height: 100,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("detail_provider_text1".tr, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      SizedBox(height: 15),
                      Text("detail_provider_text2".tr, style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 5),
                      Text(
                        state['location']['provider_address'], 
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 13, 
                          color: themeController.isDarkMode.value ? Colors.white70 : Colors.grey.shade700
                        )
                      ),
                      SizedBox(height: 5),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(margin: EdgeInsets.only(top: 10), child: Text("detail_provider_text3".tr, style: TextStyle(fontWeight: FontWeight.bold))),
                          SizedBox(width: 10),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                color: state['operational_hour']['open_today'] == 'Open' ? Colors.green : Colors.red
                              )
                            ),
                            child: Text(
                              state['operational_hour']['open_today'], 
                              style: TextStyle(
                                color: state['operational_hour']['open_today'] == 'Open' ? Colors.green : Colors.red
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text(
                            "${state['operational_hour']['schedule'][0]['day_start']} - ${state['operational_hour']['schedule'][0]['day_end']}", 
                            style: TextStyle(
                              fontSize: 13, 
                              color: themeController.isDarkMode.value ? Colors.white70 : Colors.grey.shade700
                            )
                          ),
                          SizedBox(width: 15),
                          Text(
                            "08:00 - 23:00", 
                            style: TextStyle(
                              fontSize: 13, 
                              color: themeController.isDarkMode.value ? Colors.white70 : Colors.grey.shade700
                            )
                          )
                        ],
                      ),
                      SizedBox(height: 15),
                      Text("detail_provider_text4".tr, style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 5),
                      Text(
                        state['profile']['provider_description'],
                        style: TextStyle(
                          fontSize: 13,
                          color: themeController.isDarkMode.value ? Colors.white70 : Colors.grey.shade700
                        ),
                      ),
                      SizedBox(height: 30),
                    ],
                  ), 
                )
              ],
            ),
          )        
        )
      )
    );
  }
}
