import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final emailC = TextEditingController(text: "");
  final passwordC = TextEditingController(text: '');
  RxString emailError = "".obs;
  RxString passwordError = "".obs;
  RxBool loginLoading = false.obs;

  @override
  void onClose() {
    emailC.dispose();
    passwordC.dispose();
    super.onClose();
  }
}
