import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/error-text.dart';
import 'package:skripsi_project/app/common-widgets/input-form.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/modules/login/controllers/login_controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class LoginFormWidget extends GetView<LoginController> {
  LoginFormWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final authController = Get.find<AuthController>();
    
    return Form(
      child: Container(
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InputForm(
              controller: controller.emailC,
              label: "Email",
              placeholder: 'login_placeholder'.tr,
              icon: Icons.person_outline_outlined,
            ),
            Obx(() => controller.emailError.value != "" ? ErrorText(textError: "login_error_email".tr, icon: Icons.warning_outlined) : Container()),
            SizedBox(height: 30),
            InputForm(
              controller: controller.passwordC,
              label: "Password",
              placeholder: 'login_password1'.tr,
              icon: Icons.lock_outline,
              obscureText: true,
            ),
            Obx(() => controller.passwordError.value != "" ? ErrorText(textError: "login_error_password".tr, icon: Icons.warning_outlined) : Container()),
            SizedBox(height: 10),
            Align(
              alignment: Alignment.centerRight,
              child: TextButton(
                onPressed: () => Get.toNamed(Routes.FORGOT_PASSWORD), 
                style: TextButton.styleFrom(
                  foregroundColor: AppTheme.primaryColorDark
                ),
                child: Text(
                  "login_password2".tr, 
                  style: TextStyle(
                    color: AppTheme.primaryColorDark,
                  ),
                )
              )
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  authController.login(
                    controller.emailC.text, 
                    controller.passwordC.text
                  );
                }, 
                child: Text("login_button".tr),
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppTheme.primaryColorDark
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}