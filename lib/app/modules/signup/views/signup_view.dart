import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/form-header-widget.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
// import 'package:profile/app/common_widgets/formHeader_widget.dart';
// import 'package:profile/app/common_widgets/signup_widget.dart';
// import 'package:profile/app/routes/app_pages.dart';
// import 'package:profile/constants/color.dart';
import 'package:skripsi_project/app/modules/signup/widgets/sign-form-widget.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/signup_controller.dart';

class SignupView extends GetView<SignupController> {
  const SignupView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final authC = Get.put(AuthController());

    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FormHeaderWidget(
                    image: "assets/images/signup.svg", 
                    title: "signup_title".tr, 
                    heightBox: 25,
                    subTitle: "signup_desc".tr
                  ),
                  SignUpFormWidget(),
                  Align(alignment: Alignment.center, child: Text("login_or".tr)),
                  SizedBox(height: 20),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: OutlinedButton.icon(
                      icon: Image(image: AssetImage("assets/images/google.png"), width: 20,),
                      onPressed: () => authC.loginWithGoogle(), 
                      label: Text("signup_provider".tr, style: TextStyle(
                        color: themeController.isDarkMode.value ? Colors.white : Colors.black,              
                      ),),   
                      style: OutlinedButton.styleFrom(
                        side: BorderSide(color: themeController.isDarkMode.value ? Colors.white : Colors.black),
                        foregroundColor: AppTheme.primaryColorDark
                      ),         
                    ),
                  ),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("signup_footer_desc".tr),
                      TextButton(
                        onPressed: () => Get.toNamed(Routes.LOGIN), 
                        child: Text("login_title".tr, style: TextStyle(color: AppTheme.primaryColorDark)),
                        style: TextButton.styleFrom(
                          foregroundColor: AppTheme.primaryColorDark
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
