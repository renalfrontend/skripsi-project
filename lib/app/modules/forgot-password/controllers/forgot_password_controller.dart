import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPasswordController extends GetxController {
  final emailC = TextEditingController(text: "");
  RxString emailError = "".obs;
  RxBool loadingForgotPassword = false.obs;

  @override
  void onClose() {
    emailC.dispose();
    super.onClose();
  }
}
