import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignupController extends GetxController {
  final fullnameC = TextEditingController(text: "");
  final emailC = TextEditingController(text: "");
  final passwordC = TextEditingController(text: "");
  RxString fullnameError = "".obs;
  RxString emailError = "".obs;
  RxString passwordError = "".obs;
  RxBool signupLoading = false.obs;

  @override
  void onClose() {
    fullnameC.dispose();
    emailC.dispose();
    passwordC.dispose();
    super.onInit();
  }
}
