part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const WELCOME = _Paths.WELCOME;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const FORGOT_PASSWORD = _Paths.FORGOT_PASSWORD;
  static const PROFILE = _Paths.PROFILE;
  static const EDIT_PROFILE = _Paths.EDIT_PROFILE;
  static const INFORMATION = _Paths.INFORMATION;
  static const SEARCH_PROVIDER_PAGE = _Paths.SEARCH_PROVIDER_PAGE;
  static const DETAIL_PROVIDER = _Paths.DETAIL_PROVIDER;
  static const SEARCH_MYTARGET_PAGE = _Paths.SEARCH_MYTARGET_PAGE;
  static const INPUT_TARGET = _Paths.INPUT_TARGET;
  static const CONFIRMATION_TARGET = _Paths.CONFIRMATION_TARGET;
  static const DETAIL_MYTARGET = _Paths.DETAIL_MYTARGET;
  static const HISTORY = _Paths.HISTORY;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const WELCOME = '/welcome';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const FORGOT_PASSWORD = '/forgot-password';
  static const PROFILE = '/profile';
  static const EDIT_PROFILE = '/edit-profile';
  static const INFORMATION = '/information';
  static const SEARCH_PROVIDER_PAGE = '/search-provider-page';
  static const DETAIL_PROVIDER = '/detail-provider';
  static const SEARCH_MYTARGET_PAGE = '/search-mytarget-page';
  static const INPUT_TARGET = '/input-target';
  static const CONFIRMATION_TARGET = '/confirmation-target';
  static const DETAIL_MYTARGET = '/detail-mytarget/:id';
  static const HISTORY = '/history';
}
