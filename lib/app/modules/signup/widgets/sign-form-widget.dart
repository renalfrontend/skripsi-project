import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/error-text.dart';
import 'package:skripsi_project/app/common-widgets/input-form.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/modules/signup/controllers/signup_controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class SignUpFormWidget extends GetView<SignupController>  {
  const SignUpFormWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final authController = Get.find<AuthController>();

    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InputForm(
              controller: controller.fullnameC,
              label: "signup_label".tr,
              placeholder: 'signup_placeholder_name'.tr,
              icon: Icons.person_outline,
            ),
            Obx(
              () => controller.fullnameError.value != "" ? 
              ErrorText(textError: "signup_error_fullname".tr, icon: Icons.warning_outlined) : Container()
            ),
            SizedBox(height: 10),
            InputForm(
              controller: controller.emailC,
              label: "Email",
              placeholder: 'signup_placeholder_email'.tr,
              icon: Icons.email_outlined,
            ),
            Obx(
              () => controller.emailError.value != "" ? 
              ErrorText(textError: "signup_error_email".tr, icon: Icons.warning_outlined) : Container()
            ),
            SizedBox(height: 10),
            InputForm(
              controller: controller.passwordC,
              label: "Password",
              placeholder: 'signup_placeholder_password'.tr,
              icon: Icons.lock_outlined,
              obscureText: true,
            ),
            Obx(
              () => controller.passwordError.value != "" ? 
              ErrorText(textError: "signup_error_password".tr, icon: Icons.warning_outlined) : Container()
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () {
                  authController.signup(
                    controller.fullnameC.text,
                    controller.emailC.text, 
                    controller.passwordC.text
                  );
                }, 
                child: Obx(
                  () => controller.signupLoading.value ? 
                  SizedBox(height: 20, width: 20, child: CircularProgressIndicator(strokeWidth: 2)) : 
                  Text("signup_title".tr)),
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppTheme.primaryColorDark
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
