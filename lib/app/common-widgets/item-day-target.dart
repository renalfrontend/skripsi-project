import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/modules/detail-mytarget/controllers/detail_mytarget_controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemDayTarget extends StatelessWidget {
  ItemDayTarget({
    super.key,
    required this.type,
    this.day,
    this.uniqIdForNotification,
    this.textOlahraga1="",
    this.textMakan1="",
    this.textMakan2="",
    this.textMakan3="",
    this.textMakanTime1="",
    this.textMakanTime2="",
    this.textMakanTime3="",
    this.descMakan1="",
    this.descMakan2="",
    this.descMakan3="",
    this.numbEndItem,
    this.totalTarget
  });
  
  String type;
  int? day;
  int? uniqIdForNotification = 0;
  String? textOlahraga1;
  String? textMakan1;
  String? textMakan2;
  String? textMakan3;
  String? textMakanTime1;
  String? textMakanTime2;
  String? textMakanTime3;
  String? descMakan1;
  String? descMakan2;
  String? descMakan3;
  String? numbEndItem;
  int? totalTarget;

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    DetailMytargetController detailTargetController = Get.put(DetailMytargetController());

    return type == 'pola-makan' || type == 'dietary-habit' ? Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 20),
          child: day != null ? Text("${'detail_target_item_title'.tr} $day", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)) : Container(),
        ),
        Container(
          margin: EdgeInsets.only(top: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("\u2022 ${(textMakan1 as String).tr}              : ", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 5),
                  Text("      ($textMakanTime1)")
                ],
              ),
              Expanded(child: Text("${(descMakan1 as String).tr}"))
            ],
          ),
        ),

         Container(
          margin: EdgeInsets.only(top: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("\u2022 ${(textMakan2 as String).tr}           : ", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 5),
                  Text("      ($textMakanTime2)")
                ],
              ),
              Expanded(child: Text("${(descMakan2 as String).tr}"))
            ],
          ),
        ),
        
        Container(
          margin: EdgeInsets.only(top: 5, bottom: 40),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text((textMakan3 as String).tr == 'Dinner' ? "\u2022 ${(textMakan3 as String).tr}                    : " : "\u2022 ${(textMakan3 as String).tr}         : ", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 5),
                  Text("      ($textMakanTime3)")
                ],
              ),
              Expanded(child: Text("${(descMakan3 as String).tr}"))
            ],
          ),
        ),

        Container(
          child: numbEndItem == totalTarget.toString() ? Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 50),
            child: ElevatedButton(              
              onPressed: () {
                for(var i = 0; i < totalTarget!; i++){
                  AwesomeNotifications().cancel(int.parse('${uniqIdForNotification}${DateTime.now().add(Duration(days: i)).day}'));
                }
                detailTargetController.tabController1.index = 0;
                detailTargetController.updateStatusTarget();
              },
              style: ElevatedButton.styleFrom(backgroundColor: themeController.isDarkMode.value ? AppTheme.primaryColorDark : HexColor(Get.arguments)),
              child: Text("button_finish_target".tr),
            )
          ) : Container()
        )
      ],
    ) : Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: day != null ? Text("${'detail_target_item_title'.tr} $day", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)) : Container(),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("\u2022 ${(textOlahraga1 as String).tr}"),
              ],
            ),
          ),
          Container(
            child: numbEndItem == totalTarget.toString() ? Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 50),
              child: ElevatedButton(              
                onPressed: () {
                  for(var i = 0; i < totalTarget!; i++){
                    AwesomeNotifications().cancel(int.parse('${uniqIdForNotification}${DateTime.now().add(Duration(days: i)).day}'));
                  }
                  detailTargetController.tabController1.index = 0;
                  detailTargetController.updateStatusTarget();
                },
                style: ElevatedButton.styleFrom(backgroundColor: themeController.isDarkMode.value ? AppTheme.primaryColorDark : HexColor(Get.arguments)),
                child: Text("button_finish_target".tr),
              )
            ) : Container()
          )
        ],
      ),
    );
  }
}