import 'package:get/get.dart';

import '../controllers/search_mytarget_page_controller.dart';

class SearchMytargetPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchMytargetPageController>(
      () => SearchMytargetPageController(),
    );
  }
}
