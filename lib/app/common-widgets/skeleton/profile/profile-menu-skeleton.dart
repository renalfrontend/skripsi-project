import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

class ProfileMenuSkeleton extends StatelessWidget {
  ProfileMenuSkeleton({this.endIconSwitch = false});

  final bool endIconSwitch;

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return ListTile(
      splashColor: Colors.transparent,
      visualDensity: VisualDensity.comfortable,
      style: ListTileStyle.drawer,
      leading: Obx(() => Shimmer.fromColors(
            baseColor: themeController.isDarkMode.value
                ? Colors.grey.shade600
                : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value
                ? Colors.white60
                : Colors.white,
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
                color: Colors.yellow,
              ),
            ),
          )),
      title: Obx(() => Shimmer.fromColors(
            baseColor: themeController.isDarkMode.value
                ? Colors.grey.shade600
                : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value
                ? Colors.white60
                : Colors.white,
            child: Container(
              width: 10,
              height: 10,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.grey,
              ),
            ),
          )),
      trailing: endIconSwitch
          ? Obx(() => Shimmer.fromColors(
                baseColor: themeController.isDarkMode.value
                    ? Colors.grey.shade600
                    : Colors.grey.shade200,
                highlightColor: themeController.isDarkMode.value
                    ? Colors.white60
                    : Colors.white,
                child: Switch(
                  value: true,
                  onChanged: (bool value) => null,
                ),
              ))
          : SizedBox(),
    );
  }
}
