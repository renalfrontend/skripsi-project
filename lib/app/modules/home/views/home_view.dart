import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/common-widgets/item-empty-my-target.dart';
import 'package:skripsi_project/app/common-widgets/item-health.dart';
import 'package:skripsi_project/app/common-widgets/item-my-target.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/providers/item-health-skeleton.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    final bodyHeight = mediaQueryHeight - MediaQuery.of(context).padding.top;
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      bottomNavigationBar: Obx(() => BottomAppBar(
        padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
        color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.home, size: 23,),
                SizedBox(height: 7),
                Text("menu_1".tr, style: TextStyle(fontSize: 12),)
              ],
            ),
            GestureDetector(
              onTap: () => Get.toNamed(Routes.SEARCH_MYTARGET_PAGE),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.track_changes, size: 23,),
                  SizedBox(height: 7),
                  Text("menu_2".tr, style: TextStyle(fontSize: 12),)
                ],
              ),
            ),
            GestureDetector(
              onTap: () => Get.toNamed(Routes.SEARCH_PROVIDER_PAGE),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.local_hospital, size: 23,),
                  SizedBox(height: 7),
                  Text("menu_3".tr, style: TextStyle(fontSize: 12),)
                ],
              ),
            ),
            GestureDetector(
              onTap: () => Get.toNamed(Routes.HISTORY),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.history, size: 23,),
                  SizedBox(height: 7),
                  Text("menu_4".tr, style: TextStyle(fontSize: 12),)
                ],
              ),
            )
          ],
        ),
      )),
      body: Obx(() => Container(
        width: MediaQuery.of(context).size.width,  
        height: bodyHeight,
        color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : AppTheme.backgroundColor,      
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 40),
          child: Column(
            children: [
              // Header 
              Container(
                child: Stack(
                  children: [
                    Container(
                      height: bodyHeight * 0.3,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/bg-home-1.jpg"),
                      ),
                    ),

                    Positioned(
                      top: 50,
                      right: 20,
                      child: GestureDetector(
                        onTap: () => Get.toNamed(Routes.PROFILE),
                        child: SizedBox(
                          width: 55,
                          height: 55,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              border: Border.all(
                                color: Color.fromARGB(223, 255, 255, 255),
                                width: 3,
                              )
                            ),
                            child: StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                              stream: controller.streamHome(),
                              builder:(context, snapshot) {
                                final data = snapshot.data?.data();
                                if(snapshot.connectionState == ConnectionState.active){
                                  return ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Container(
                                        color: Colors.white,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            imageUrl: data?['photo'] == "" || data?['photo'] == null ? "https://icones.pro/wp-content/uploads/2021/05/symbole-de-l-homme-violet.png" : data?['photo'],
                                            progressIndicatorBuilder: (context, url, downloadProgress) => CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark),
                                            errorWidget: (context, url, error) => Image.network("https://cdn-icons-png.flaticon.com/512/42/42994.png"),
                                          ),
                                        ),
                                      ),
                                  );
                                }else if(snapshot.connectionState == ConnectionState.waiting){
                                  return ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image(
                                      fit: BoxFit.cover,
                                      image: NetworkImage("https://icones.pro/wp-content/uploads/2021/05/symbole-de-l-homme-violet.png")
                                    ),
                                  );
                                }else {
                                  return Container();
                                }
                              }
                            )                                                                                  
                          ),
                        ),
                      ),
                    ),

                    Positioned(
                      width: mediaQueryWidth * 0.6,
                      bottom: 20,
                      left: 20,
                      child: Text(
                        "home_title".tr,
                        style: GoogleFonts.poppins(
                          fontSize: 25,
                          color: Color.fromARGB(223, 255, 255, 255),
                          fontWeight: FontWeight.bold,
                          height: 1.2,
                          shadows: [
                            Shadow(
                              offset: Offset(1.0, 2.0),
                              blurRadius: 20,
                              color: Color.fromARGB(255, 0, 0, 0),
                            ),                          
                          ]
                        )
                      )
                    )
                  ],
                )
              ),

              Container(
                width: mediaQueryWidth,
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    // Item My Target
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "home_myTarget_title".tr, 
                              style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.w700
                              ),
                            ),
                            GestureDetector(
                              onTap: () => Get.toNamed(Routes.SEARCH_MYTARGET_PAGE),
                              child: Row(
                                children: [
                                  Text(
                                    "home_link".tr,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                      color: AppTheme.primaryColorDark
                                    ),
                                  ),
                                  Icon(Icons.arrow_right_alt, color: AppTheme.primaryColorDark,)
                                ],
                              ),
                            ),
                          ],
                        ),

                        Container(
                          child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                            stream: controller.streamNewTarget(),
                            builder: (context, snapshot) {
                              final dataTarget = snapshot.data?.docs;

                              if(snapshot.connectionState == ConnectionState.active){
                                if(dataTarget?.length != 0){
                                  return GestureDetector(
                                    onTap: () {
                                      Get.toNamed('/detail-mytarget/${dataTarget![0]['id']}', arguments: dataTarget[0]['colorTarget']);
                                      GetStorage().write("tabBarLength", dataTarget[0]['polaDetailTarget']);
                                      GetStorage().write('tabBarIndex', dataTarget[0]['tabBarIndex']);
                                    },
                                    child: ItemMyTarget(
                                      titleTarget: dataTarget?[0]['targetName'],
                                      desc1: dataTarget?[0]['targetPolaDetail'],
                                      desc2: dataTarget?[0]['targetWaktu'],
                                      color: dataTarget?[0]['colorTarget'],
                                    ),
                                  );
                                }else {
                                  return ItemEmptyMyTarget(mediaQueryWidth: mediaQueryWidth, themeController: themeController);
                                }
                              }

                              return Shimmer.fromColors(
                                baseColor: themeController.isDarkMode.value
                                    ? Colors.grey.shade600
                                    : Colors.grey.shade200,
                                highlightColor: themeController.isDarkMode.value
                                    ? Colors.white60
                                    : Colors.white,
                                child: Container(
                                  width: mediaQueryWidth,
                                  height: 100,
                                  margin: EdgeInsets.only(top: 20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    
                    // Item Fasilitas kesehatan
                    Container(
                      margin: EdgeInsets.only(top: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "home_myProvider_title".tr, 
                                style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w700
                                ),
                              ),
                              GestureDetector(
                                onTap: () => Get.toNamed(Routes.SEARCH_PROVIDER_PAGE),
                                child: Row(
                                  children: [
                                    Text(
                                      "home_link".tr,
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                        color: AppTheme.primaryColorDark
                                      ),
                                    ),
                                    Icon(Icons.arrow_right_alt, color: AppTheme.primaryColorDark,)
                                  ],
                                ),
                              ),
                            ],
                          ),
                    
                          Container(
                            width: mediaQueryWidth,
                            height: 160,
                            margin: EdgeInsets.only(top: 15),
                            child: controller.obx((state) => ListView.separated(
                                itemCount: (state!['data'] as List<dynamic>).length,
                                clipBehavior: Clip.none,
                                scrollDirection: Axis.horizontal,
                                separatorBuilder: (context, index) => SizedBox(width: 20),
                                itemBuilder:(context, index) {
                                  final data = state['data'][index];

                                  return ItemProviderHealth(
                                    providerId: data['provider_id'].toString(),
                                    providerBanner: data['provider_banner'] != null ? data['provider_banner'] : "https://t3.ftcdn.net/jpg/01/38/48/40/360_F_138484065_1enzXuW8NlkppNxSv4hVUrYoeF8qgoeY.jpg",
                                    providerName: data['provider_name'] != null ? data['provider_name'] : "Tidak Tersedia",
                                    providerLocation: data['location']['location_gmaps'] != null ? data['location']['location_gmaps'] : "-"
                                  );
                                  // return ItemHealthSkeleton();
                                },
                              ),
                              onLoading: ListView.separated(
                                itemCount: 10,
                                clipBehavior: Clip.none,
                                scrollDirection: Axis.horizontal,
                                separatorBuilder: (context, index) => SizedBox(width: 20),
                                itemBuilder:(context, index) {
                                  return ItemHealthSkeleton();
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              
            ],
          ),
        ),
      ))
    );
  }
}