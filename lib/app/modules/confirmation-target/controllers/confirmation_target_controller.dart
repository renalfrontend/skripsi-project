import 'dart:math';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/data/pola-makan.dart';
import 'package:skripsi_project/app/data/pola-olahraga.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/firestore.dart';

class ConfirmationTargetController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxString targetName = "".obs;
  RxString targetPola = "".obs;
  RxString targetPolaDetail = "".obs;
  RxString targetWaktu = "".obs;
  RxString polaDescription = "".obs;
  Rx<DateTime> createDateTarget = DateTime.now().obs;
  Rx<DateTime> endDateTarget = DateTime.now().obs;
  RxMap polaDetailTarget = {}.obs;
  Rx<String> colorTarget = "".obs;
  late var idUser;

  void createTarget() async {
    DateTime idTarget = DateTime.now();
    String userName = "";
    List<Map<String, dynamic>> tabBarIndex = [];
    Map<String, dynamic>? dataPolaUser = {};

    await firestore
      .collection('users')
      .doc(idUser)
      .collection('pola-list')
      .doc(idTarget.toString().replaceAll(" ", ""))      
      .set({
        'id': idTarget.toString().replaceAll(" ", ""),
        "uniqIdForNotification": Random().nextInt(1000000),
        "tag-id": targetPola.value.tr.toLowerCase().split(" ").join('-'),
        "targetName": targetName.value,
        "targetPola": targetPola.value,
        "targetPolaDetail": targetPolaDetail.value,
        "targetWaktu": targetWaktu.value,
        "polaDescription": polaDescription.value,
        "polaDetailTarget": polaDetailTarget,
        'colorTarget': colorTarget.value,
        "targetNotification": int.parse("${polaDetailTarget.length}"),
        "createDateTarget": DateTime.now(),
        "endDateTarget": endDateTarget.value,
        "targetCompleted": "text_not_finish",
      });

      await getUserName().then((value) {
        final data = value.data();
        userName = (data?['fullname'] as String).split(' ')[0];
      });

      await firestore
        .collection('users')
        .doc(GetStorage().read('id-user'))
        .collection('pola-list')
        .doc(idTarget.toString().replaceAll(" ", ""))
        .get()
        .then((value) => {
          dataPolaUser = value.data()
        });

      for(var i = 0; i < dataPolaUser?['targetNotification']; i++){
        AwesomeNotifications().createNotification(
          content: NotificationContent(
            id: int.parse('${Random().nextInt(1000000)}${DateTime.now().add(Duration(days: i)).day}'), 
            channelKey: 'fit_reminder_scheduled',
            summary: targetName.value,
            title: "${targetPola.value.toString().tr.split(' ')[1]} hari ke ${i+1}",
            body: "Hello ${userName}, sudah waktunya jadwal ${targetPola.value.toLowerCase().toString().tr.split(' ')[1]} kamu hari ini. Semoga kamu bisa konsisten :)",
            notificationLayout: NotificationLayout.BigText,
            payload: {
              "nameRoute": "/detail-mytarget/${idTarget.toString().replaceAll(" ", "")}",
              "arguments": "${colorTarget.value}",
              "targetNotification": "${dataPolaUser?['targetNotification']}",
              "idUser": "${GetStorage().read('id-user')}",
              "idPolaUser": "${idTarget.toString().replaceAll(" ", "")}",
              "indexNotificationActive": "${i + 1}"
            },
          ),
          schedule: NotificationCalendar(
            timeZone: AwesomeNotifications.localTimeZoneIdentifier,
            second: 0,
            millisecond: 0,
            day: DateTime.now().add(Duration(days: i)).day,
            hour: i == 0 ? DateTime.now().hour : 07,
            minute: i == 0 ? DateTime.now().minute + 1 : 00
          )
        );

        tabBarIndex.add({
          "${DateTime.now().add(Duration(days: i)).day}" : i
        });        
      }

      await GetStorage().write("tabBarLength", polaDetailTarget);
      await GetStorage().write("tabBarIndex", tabBarIndex);

      await firestore
      .collection('users')
      .doc(idUser)
      .collection('pola-list')
      .doc(idTarget.toString().replaceAll(" ", ""))      
      .update({
        "tabBarIndex": tabBarIndex
      });

      Get.offAllNamed(Routes.SEARCH_MYTARGET_PAGE);
  }

  @override
  void onInit() {
    dynamic arguments = Get.arguments;
    idUser = GetStorage().read('id-user');

    targetName.value = arguments['inputTargetName'];
    targetPola.value = arguments['inputPola'];
    targetPolaDetail.value = arguments['inputPolaMakanOrOlahraga'];
    targetWaktu.value = arguments['inputWaktuTarget'];    
    polaDescription.value = arguments['polaDescription'];
    colorTarget.value = arguments['warnaTarget'];
    polaDetailTarget.value = 
    arguments['inputPola'].toString().tr == "Pola Makan" || arguments['inputPola'].toString().tr == "Dietary habit" ? 
    polaMakanType[0][arguments['inputWaktuTarget'].toString().tr][arguments['inputPolaMakanOrOlahraga'].toString().tr] : 
    polaOlahragaType[0][arguments['inputWaktuTarget'].toString().tr][arguments['inputPolaMakanOrOlahraga'].toString().tr];
    endDateTarget.value = DateTime.now().add(Duration(days: polaDetailTarget.value.length));

    super.onInit();
  }

  @override
  void dispose() {
    targetName.value = "";
    targetPola.value = "";
    targetPolaDetail.value = "";
    targetWaktu.value = "";

    super.dispose();
  }
}
