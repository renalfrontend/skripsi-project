import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/data/pola-makan.dart';
import 'package:skripsi_project/app/data/pola-olahraga.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';

class InputTargetController extends GetxController {
  late TextEditingController inputTargetName;
  Rx<String> color = "#A020F0".obs;
  RxString inputPola = "".obs;
  RxBool streamInputPola = false.obs;
  RxString inputPolaMknOrolhrg = "".obs;
  RxString inputWaktuTarget = "".obs;

  RxString inputTargetNameError = "".obs;
  RxString inputPolaError = "".obs;
  RxString inputPolaMknOrolhrgError = "".obs;
  RxString inputWaktuTargetError = "".obs;

  void changeColor(String myColor){
    color.value = myColor;
  }

  void submitTargetForm(String inputTargetName, String inputPola, String inputPolaMknOrolhrg, String inputWaktuTarget, String colorTargetInput){
    if(inputTargetName.isEmpty) inputTargetNameError.value = "Nama target harus diisi";
    if(inputPola.isEmpty) inputPolaError.value = "Nama target harus diisi";
    if(inputPolaMknOrolhrg.isEmpty) inputPolaMknOrolhrgError.value = "Nama target harus diisi";
    if(inputWaktuTarget.isEmpty) inputWaktuTargetError.value = "Nama target harus diisi";
    if(inputTargetName.isNotEmpty && inputPola.isNotEmpty && inputPolaMknOrolhrg.isNotEmpty && inputWaktuTarget.isNotEmpty){
      Get.toNamed(
        Routes.CONFIRMATION_TARGET,
        arguments: {
          "inputTargetName": inputTargetName,
          "inputPola": inputPola,
          "inputPolaMakanOrOlahraga": inputPolaMknOrolhrg,
          "inputWaktuTarget": inputWaktuTarget,
          "warnaTarget": colorTargetInput,
          "polaDescription": inputPola.tr == "Dietary habit" || inputPola.tr == "Pola Makan" ? polaMakanType[0][inputPolaMknOrolhrg.tr]['description'] : polaOlahragaType[0][inputPolaMknOrolhrg.tr]['description']
        }
      );
    }
  }

  void fillForm(Object value, RxString? input, RxString? errorText){
    input!.value = value != (value as String) ? "" : value;
    errorText!.value = "";
    inputTargetNameError.value = "";
  }

  @override
  void onInit() {
    inputTargetName = TextEditingController(text: "");

    ever(inputPola, (callback) {
      streamInputPola.value = true;
      inputPolaMknOrolhrg.value = "";
    });

    super.onInit();
  }

  @override
  void dispose() {
    inputTargetName.dispose();
    inputPola.value = "";
    streamInputPola.value = false;
    inputWaktuTarget.value = "";
    inputTargetNameError.value = "";
    inputPolaError.value = "";
    inputPolaMknOrolhrgError.value = "";
    inputWaktuTargetError.value = "";

    super.dispose();
  }
}
