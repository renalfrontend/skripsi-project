import 'dart:convert';
import 'dart:io';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class NotificationController extends GetxController {

  static Future<void> onActionReceivedMethod(ReceivedAction receivedAction) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    List<Map<String, dynamic>> tabBarIndex = [];
    
    if(receivedAction.channelKey == 'fit_reminder' || receivedAction.channelKey == 'fit_reminder_scheduled'){
      AwesomeNotifications().getGlobalBadgeCounter().then((value) => {
        AwesomeNotifications().setGlobalBadgeCounter(value - 1)
      });
    }

    final payload = receivedAction.payload ?? {};

    // await GetStorage().write("tabBarIndex", payload['tabBarIndex']);

    if(Platform.isIOS){
      if(
        "${payload['type']}" == 'from-history' || "${payload['type']}" == "from-login"
        && 
        DateTime.now().day == DateTime.utc(DateTime.now().year, DateTime.now().month, (payload['dayRemoveByUser'] != null ? int.parse("${payload['dayRemoveByUser']}") : 1)).day
        &&
        payload['dayRemoveByUser'] != null
      ){
        Get.toNamed("${payload['nameRoute']}", arguments: "${payload['arguments']}");
      }else {
        // decrement targetNotification user
        await firestore
          .collection('users')
          .doc("${payload['idUser']}")
          .collection('pola-list')
          .doc('${payload['idPolaUser']}')
          .update({
            "targetNotification": int.parse("${payload['targetNotification']}") - 1,
            "indexNotificationActive": int.parse("${payload['indexNotificationActive']}")
          });

        Get.toNamed("${payload['nameRoute']}", arguments: "${payload['arguments']}");
      }
    } else Get.toNamed("${payload['nameRoute']}", arguments: "${payload['arguments']}");
  }

  static Future<void> onNotificationDisplayedMethod(ReceivedNotification receivedNotification) async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    final payload = receivedNotification.payload ?? {};

    if(
      "${payload['type']}" == 'from-history' || "${payload['type']}" == "from-login"
      && 
      DateTime.now().day == DateTime.utc(DateTime.now().year, DateTime.now().month, (payload['dayRemoveByUser'] != null ? int.parse("${payload['dayRemoveByUser']}") : 1)).day
      &&
      payload['dayRemoveByUser'] != null
    ){
      return;
    }else {
      // decrement targetNotification user
      await firestore
        .collection('users')
        .doc("${payload['idUser']}")
        .collection('pola-list')
        .doc('${payload['idPolaUser']}')
        .update({
          "targetNotification": int.parse("${payload['targetNotification']}") - 1,
          "indexNotificationActive": int.parse("${payload['indexNotificationActive']}")
        });
    }
  }

  @override
  void onInit() {
    AwesomeNotifications().initialize(
      null, 
      [ 
        NotificationChannel(
          channelKey: "fit_reminder", 
          channelName: "Fit Reminder", 
          channelDescription: "Notification for fit reminder",
          importance: NotificationImportance.High,
          channelShowBadge: true
        ),
        NotificationChannel(
          channelKey: 'fit_reminder_scheduled', 
          channelName: 'Fit Reminder Scheduled', 
          channelDescription: "Scheduled Notifications for Fit Reminder",
          importance: NotificationImportance.High,
          channelShowBadge: true,
          defaultColor: AppTheme.primaryColorDark
        )
      ],
      debug: true
    );

    AwesomeNotifications().setListeners(
      onActionReceivedMethod: onActionReceivedMethod,
      onNotificationDisplayedMethod: onNotificationDisplayedMethod,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
