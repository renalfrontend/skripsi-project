import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/export.dart';

Uint8List randomBytes(int length) {
  final random = Random.secure();
  final bytes = Uint8List(length);
  for (int i = 0; i < length; i++) {
    bytes[i] = random.nextInt(256);
  }
  return bytes;
}

String encryptKey(List<int> keyBytes, List<int> ivBytes) {
  final key = base64.encode(keyBytes);
  final iv = base64.encode(ivBytes);
  final keyIV = '$key|$iv';

  final pemPubKey = '''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnEyeWIx7MUd4gpvX/bAd+VARfFF8zdHjS1/ctyp8yVTWHtvhbL9eIMW7ZWx8bzqYp4Ad7Z5boqftl/OcbmH6sUGncQjj/8U6o28s4Opj8lj7e3n6KC3zbEDFj13OQ5DazcTauuGUFovNGreiAFV+Tio4hVHjQDiatVW9IYusrg/FUbG0Ore4gCto0aGgQw4ZMlA66tQ23Qoj+Bg0Lk7QesRyY9JiazFSyFRTrK7USTFncunVjkcl5WIuqAIzHZJib0Bq2oI3Hd5BGnzeyz/R8gDMYFhJwBAUIt1pIcC5v5t/s2hMvluLQbIh6LbgxV/PcmQ3LZIdvVecZTjlgotBjQIDAQAB
-----END PUBLIC KEY-----''';

  final parser = RSAKeyParser();
  final publicKey = parser.parse(pemPubKey) as RSAPublicKey;

  final encrypter = PKCS1Encoding(RSAEngine())
    ..init(true, PublicKeyParameter<RSAPublicKey>(publicKey));

  final encryptedKeyIV = encrypter.process(Uint8List.fromList(utf8.encode(keyIV)));

  return base64.encode(encryptedKeyIV);
}

String encryptData(String data, Uint8List key, Uint8List iv) {
  final encrypter = Encrypter(
    AES(Key(Uint8List.fromList(key)), mode: AESMode.cbc, padding: 'PKCS7'),
  );
  final encrypted = encrypter.encrypt(data, iv: IV(Uint8List.fromList(iv)));
  return base64.encode(encrypted.bytes);
}

String encryptBodyProvider(Map<String, dynamic> params){
  String stringifiedString = jsonEncode(params);

  final keyBytes = randomBytes(32);
  final ivBytes = randomBytes(16);

  final encryptKeys = encryptKey(keyBytes, ivBytes);
  final encryptDatas = encryptData(stringifiedString, keyBytes, ivBytes);

  return "${encryptKeys}|${encryptDatas}";
}