import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemHealthSkeleton extends StatelessWidget {
  const ItemHealthSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(20),
      child: Container(
        width: 170,
        height: 160,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
        ),
        child: Column(
          children: [
            Obx(() => Shimmer.fromColors(
              baseColor: themeController.isDarkMode.value
                    ? Colors.grey.shade600
                    : Colors.grey.shade200,
                highlightColor: themeController.isDarkMode.value
                    ? Colors.white60
                    : Colors.white,
              child: Container(
                width: 170,
                height: 90,           
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                  color: Colors.white,
                ),
              ),
            )),
            Container(
              width: 170,
              height: 60,
              padding: EdgeInsets.symmetric(horizontal: 10),
              margin: EdgeInsets.only(top: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Obx(() => Shimmer.fromColors(
                      baseColor: themeController.isDarkMode.value
                          ? Colors.grey.shade600
                          : Colors.grey.shade200,
                      highlightColor: themeController.isDarkMode.value
                          ? Colors.white60
                          : Colors.white,
                      child: Container(
                        width: 170,
                        height: 5,
                        color: Colors.white,
                      ),
                    ))
                  ),
                  SizedBox(height: 7),
                  Obx(() => Shimmer.fromColors(
                    baseColor: themeController.isDarkMode.value
                        ? Colors.grey.shade600
                        : Colors.grey.shade200,
                    highlightColor: themeController.isDarkMode.value
                        ? Colors.white60
                        : Colors.white,
                    child: Container(
                      width: 100,
                      height: 5,
                      color: Colors.white,
                    ),
                  )),
                  SizedBox(height: 7),
                  Obx(() => Shimmer.fromColors(
                    baseColor: themeController.isDarkMode.value
                        ? Colors.grey.shade600
                        : Colors.grey.shade200,
                    highlightColor: themeController.isDarkMode.value
                        ? Colors.white60
                        : Colors.white,
                    child: Container(
                      width: 120,
                      height: 5,
                      color: Colors.white,
                    ),
                  ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}