import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/error-text.dart';
import 'package:skripsi_project/app/common-widgets/form-header-widget.dart';
import 'package:skripsi_project/app/common-widgets/input-form.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/modules/forgot-password/controllers/forgot_password_controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ForgetPasswordView extends GetView<ForgotPasswordController> {
  const ForgetPasswordView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authController = Get.find<AuthController>();

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                SizedBox(height: 80,),
                FormHeaderWidget(
                  image: "assets/images/login.svg", 
                  title: "forget_ps_title".tr, 
                  subTitle: "forget_ps_desc".tr,
                  heightBox: 40,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 30),
                Form(
                  child: Column(
                    children: [
                      InputForm(
                        controller: controller.emailC,
                        label: "Email", 
                        placeholder: "forget_placeholder_input".tr, 
                        icon: Icons.email_outlined
                      ),
                      Obx(
                        () => controller.emailError.value != "" ? 
                        ErrorText(textError: "forget_password_error".tr, icon: Icons.warning_outlined) : Container()
                      ),
                      SizedBox(height: 20),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () {
                            authController.resetPassword(
                              controller.emailC.text
                            );
                          }, 
                          child: Obx(
                            () => controller.loadingForgotPassword.value ? 
                            SizedBox(height: 20, width: 20, child: CircularProgressIndicator(strokeWidth: 2)) : 
                            Text("forget_btn".tr)),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: AppTheme.primaryColorDark
                          ),
                        ),
                      )
                    ],
                  )
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}
