import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/profile-menu-widget.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/profile/appbar-skeleton.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/profile/profile-header-skeleton.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/profile/profile-menu-skeleton.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  const ProfileView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final authController = Get.put(AuthController());

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Obx(() => themeController.countdown.value == 0 ||
                    themeController.countdown.value >= 3 
                ? AppBar(
                    shadowColor: Colors.transparent,
                    title: Text("profile_title".tr,
                        style: Theme.of(context).textTheme.titleLarge, textAlign: TextAlign.center,),
                    leading: IconButton(
                      icon: Icon(
                        LineAwesomeIcons.angle_left,
                        color: themeController.isDarkMode.value
                            ? Colors.white70
                            : Colors.black87,
                      ),
                      onPressed: () => Get.back(),
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                    ),
                  )
                : AppBarSkeleton()),
            Container(
              padding: EdgeInsets.all(30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Obx(() => themeController.countdown.value == 0 || themeController.countdown.value >= 3 ? 
                    Column(
                      children: [                        
                        StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                          stream: controller.streamProfile(),
                          builder:(context, snapshot) {
                            final data = snapshot.data?.data();
                            if(snapshot.connectionState == ConnectionState.active){
                              return Column(
                                children: [
                                  SizedBox(
                                    width: 105,
                                    height: 105,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: CachedNetworkImage(
                                        fit: BoxFit.cover,
                                        imageUrl: data?['photo'] == "" || data?['photo'] == null ? "https://icones.pro/wp-content/uploads/2021/05/symbole-de-l-homme-violet.png" : data?['photo'],
                                        progressIndicatorBuilder: (context, url, downloadProgress) => CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark),
                                        errorWidget: (context, url, error) => Image.network("https://cdn-icons-png.flaticon.com/512/42/42994.png"),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text("${data?['fullname']}", style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),),
                                  Text("${data?['email']}"),
                                ],
                              );
                            }else if(snapshot.connectionState == ConnectionState.waiting){
                              return Column(
                                children: [
                                  SizedBox(
                                    width: 105,
                                    height: 105,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: Image.network("${data?['photo']}", fit: BoxFit.cover,),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Text("${data?['fullname']}", style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),),
                                  Text("${data?['email']}"),
                                ],
                              );
                            }else {
                              return Container();
                            }

                          },
                        ),
                        SizedBox(height: 20),
                        SizedBox(
                          width: 200,
                          child: ElevatedButton(
                            onPressed: () => Get.toNamed(Routes.EDIT_PROFILE),
                            child: Text("ubahP_title".tr),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: AppTheme.primaryColorDark, side: BorderSide.none, shape: StadiumBorder()
                            ),
                          ),
                        )
                      ],
                    ) : ProfileHeaderSkeleton()
                  ),
                  
                  SizedBox(height: 30),
                  Divider(),
                  SizedBox(height: 20),

                  // Menu
                  // ProfileMenuSkeleton(endIconSwitch: true,),
                  Obx(() => themeController.countdown.value == 0 ||
                          themeController.countdown.value >= 3
                      ? Column(
                          children: [
                            ProfileMenuWidget(
                              title: "profile_text1".tr,
                              icon: LineAwesomeIcons.language,
                              activeImageSwitch: "assets/images/indonesia.png",
                              inActiveImageSwitch: "assets/images/english.png",
                              activeColorSwitch: Colors.red.withOpacity(0.5),
                              inActiveColorSwitch: Colors.blue.withOpacity(0.5),
                              switchValue: themeController.IsLanguage,
                              onChangeSwitch: (value) =>
                                  themeController.changeLanguage(value),
                            ),
                            ProfileMenuWidget(
                              title: "profile_text2".tr,
                              icon: LineAwesomeIcons.paint_roller,
                              inActiveImageSwitch: "assets/images/sun.png",
                              activeImageSwitch: "assets/images/moon.png",
                              activeColorSwitch: HexColor("#244676"),
                              inActiveColorSwitch: Colors.yellow.shade400,
                              switchValue: themeController.isDarkMode,
                              onChangeSwitch: (value) =>
                                  themeController.changeTheme(value),
                            ),
                            ProfileMenuWidget(
                              title: "profile_text3".tr,
                              icon: Icons.info_outline,
                              endIconSwitch: false,
                              onTap: () => Get.toNamed(Routes.INFORMATION),
                            ),
                            SizedBox(height: 10),
                            Divider(),
                            SizedBox(height: 10),
                            ProfileMenuWidget(
                                title: "profile_text4".tr,
                                icon: Icons.logout,
                                endIconSwitch: false,
                                onTap: () => authController.logout(),
                            ),
                          ],
                        )
                      : Column(
                          children: [
                            ProfileMenuSkeleton(endIconSwitch: true),
                            ProfileMenuSkeleton(endIconSwitch: true),
                            ProfileMenuSkeleton(),
                            SizedBox(height: 10),
                            Divider(),
                            SizedBox(height: 10),
                            ProfileMenuSkeleton(),                            
                          ],
                        )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
