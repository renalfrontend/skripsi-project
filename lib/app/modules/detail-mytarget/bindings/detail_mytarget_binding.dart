import 'package:get/get.dart';

import '../controllers/detail_mytarget_controller.dart';

class DetailMytargetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailMytargetController>(
      () => DetailMytargetController(),
    );
  }
}
