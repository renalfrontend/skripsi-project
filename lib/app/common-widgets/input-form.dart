import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class InputForm extends StatelessWidget {
  InputForm({
    super.key,
    required this.label,
    required this.placeholder,
    this.icon,
    this.obscureText = false,
    this.controller,
    this.loginProvider
  });

  final String? placeholder, label, loginProvider;
  final IconData? icon;
  final bool obscureText;
  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {      
    final themeController = Get.find<ThemeController>();

    return TextField(
      enabled: loginProvider == 'google' ? false : true,
      controller: controller,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(icon != null ? 1 : 10),
        prefixIcon: icon != null ? Icon(
          icon,
          color: AppTheme.primaryColorDark,
        ) : null,
        border: OutlineInputBorder(),                
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppTheme.primaryColorDark)
        ),        
        labelText: loginProvider == 'google' ? null : label,
        labelStyle: TextStyle(
          color: Colors.grey,
        ),
        hintText: loginProvider == 'google' ? "" : placeholder,    
        filled: themeController.isDarkMode.value ? loginProvider == 'google' ? true : false : true,
        fillColor: loginProvider == 'google' ? themeController.isDarkMode.value ? Colors.deepPurpleAccent[900] : Colors.grey[100] : Colors.white    
      ),
      cursorColor: AppTheme.primaryColorDark,
      obscureText: obscureText,
    );
  }
}