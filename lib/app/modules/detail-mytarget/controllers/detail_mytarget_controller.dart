import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DetailMytargetController extends GetxController with GetTickerProviderStateMixin {
  late TabController tabController1;
  late TabController tabController2;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxInt indexTabBar = 0.obs;

  Stream<DocumentSnapshot<Map<String, dynamic>>> streamDetailTargets() {
    return firestore
      .collection('users')
      .doc(GetStorage().read('id-user'))
      .collection('pola-list')
      .doc(Get.parameters['id'])
      .snapshots();
  }

  void updateStatusTarget() async {
    await firestore
      .collection('users')
      .doc(GetStorage().read('id-user'))
      .collection('pola-list')
      .doc(Get.parameters['id'])
      .update({
        "targetCompleted": "text_finish"
      });
  }

  @override
  void onInit() {
    tabController1 = TabController(length: 2, vsync: this);
    
    if(GetStorage().read('tabBarIndex') != null){  
      (GetStorage().read('tabBarIndex') as List<dynamic>).forEach((element) {
        if(element['${DateTime.now().day}'] != null) {
          indexTabBar.value = element['${DateTime.now().day.toString()}'];
        }
      });
    }
    
    if(GetStorage().read('tabBarLength') != null){
      tabController2 = TabController(
        length: GetStorage().read('tabBarLength').length, 
        vsync: this, 
        initialIndex: indexTabBar.value
      );
    }else {
      tabController2 = TabController(
        length: 14, 
        vsync: this, 
        initialIndex: indexTabBar.value
      );
    }


    super.onInit();
  }

  @override
  void dispose() {
    tabController1.dispose();
    tabController2.dispose();
    super.dispose();
  }
}
