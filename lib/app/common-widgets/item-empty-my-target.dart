import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemEmptyMyTarget extends StatelessWidget {
  const ItemEmptyMyTarget({
    super.key,
    required this.mediaQueryWidth,
    required this.themeController,
  });

  final double mediaQueryWidth;
  final ThemeController themeController;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          Container(
            width: mediaQueryWidth,
            height: 120,
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              gradient: LinearGradient(
                colors: [
                  AppTheme.splashScreenColor.withOpacity(0.9),
                  Colors.red.withOpacity(0.7),
                ],
                tileMode: TileMode.clamp
              )
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "empty_target_title".tr,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(                                        
                          fontSize: 15,
                          fontWeight: FontWeight.w700,  
                          color: themeController.isDarkMode.value ? Colors.white : AppTheme.darkBackgroundColor
                        ),
                      ),
                      SizedBox(height: 5),
                      Flexible(
                        child: Text(
                          "empty_target_desc".tr,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(                                        
                            fontSize: 13,
                            color: themeController.isDarkMode.value ? Colors.white70 : AppTheme.darkBackgroundColor.withOpacity(0.8)
                          ),
                        ),
                      ),
                      SizedBox(height: 5),
                      GestureDetector(
                        onTap: () => Get.toNamed(Routes.INPUT_TARGET),
                        child: Text(
                          "empty_target_btn".tr,
                          style: TextStyle(                                        
                            fontSize: 13,
                            color: AppTheme.darkBackgroundColor,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Image(
                  image: AssetImage("assets/images/no-data.png")
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}