import 'dart:convert';

import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/services/providers.dart';
import 'package:skripsi_project/constants.dart';

class HomeController extends GetxController with StateMixin<Map<String, dynamic>> {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxString imageUrl = ''.obs;
  GetStorage storage = GetStorage();

  Stream<DocumentSnapshot<Map<String, dynamic>>> streamHome() {
    return firestore
      .collection("users")
      .doc(storage.read('id-user'))
      .snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> streamNewTarget() {
    return firestore
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('pola-list')
      .orderBy('id', descending: true)
      .snapshots();
  }

  void getProviders(){
    Map<String, dynamic> data;
    change(null, status: RxStatus.loading());
    double latitude = storage.read("latitude") != null ? storage.read("latitude") : defaultLatitude;
    double longtitude = storage.read("longtitude") != null ? storage.read("longtitude") : defaultLongtitude;

    Providers()
    .getProviders(coordinate: [latitude, longtitude])
    .then((value) => {
      data = json.decode(value)['data']['data'],
      change(data, status: RxStatus.success()),
    }).catchError((onError) {
      print(onError);
      change(null, status: RxStatus.error());
    });
  }

  void initState() async {
    getProviders();

    // delete otomatis history data after 7 days
    final allHistoryDataCollection = await firestore
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('history').get();

    final listDataHistory = allHistoryDataCollection.docs.map((e) => e.data()).toList();
    final listIdHistory = listDataHistory.map((e) => e['id']).toList();
    final listDateToHistory = listDataHistory.asMap().entries.map((e) => '${e.key}:${e.value['targetDateRemoveAutomatic']}').toList();

    listDateToHistory.forEach((element) {
      var targetDateRemoveAutomatic = element[2] + element[3];
      
      if(targetDateRemoveAutomatic == DateTime.now().day.toString()){
        firestore
          .collection('users')
          .doc(storage.read('id-user'))
          .collection('history')
          .doc(listIdHistory[int.parse(element[0])])
          .delete();
      }
    });
  }

  @override
  void onInit() {
    initState();
    super.onInit();
  }

  @override
  void dispose() {
    Get.delete<AuthController>();
    super.dispose();
  }
}
