List<Map<String, dynamic>> polaMakanType = [
  {
    "pola": "Pola Makan",
    "itemsInputPolaMakan": [
      'input_target_pola_makan_v',
      "input_target_pola_makan_d",
      'input_target_pola_makan_m',
      'input_target_pola_makan_me'
    ],

    "Pola makan vegetarian": {
      "description": "desc_pola_m_vegetarian"
    },
    "Vegetarian diet": {
      "description": "desc_pola_m_vegetarian"
    },
    "Pola makan dash": {
      "description": "desc_pola_m_dash"
    },
    "Dash diet": {
      "description": "desc_pola_m_dash"
    },
    "Pola makan mind": {
      "description": "desc_pola_m_mind"
    },
    "Mind diet": {
      "description": "desc_pola_m_mind"
    },
    "Pola makan mediterania": {
      "description": "desc_pola_m_mediterania"
    },
    "Mediterranean diet": {
      "description": "desc_pola_m_mediterania"
    },

    "2 Minggu": {
      "Pola makan vegetarian": polaMakanVeg2Minggu,
      "Pola makan dash": polaMakanDash2Minggu,
      "Pola makan mind": polaMakanMind2Minggu,
      "Pola makan mediterania": polaMakanMediterania2Minggu,
    },
    "2 Weeks": {
      "Vegetarian diet": polaMakanVeg2Minggu,
      "Dash diet": polaMakanDash2Minggu,
      "Mind diet": polaMakanMind2Minggu,
      "Mediterranean diet": polaMakanMediterania2Minggu
    },  
    "4 Minggu": {
      "Pola makan vegetarian": polaMakanDash4Minggu,
      "Pola makan dash": polaMakanDash4Minggu,
      "Pola makan mind": polaMakanMind4Minggu,
      "Pola makan mediterania": polaMakanMediterania4Minggu,
    },
    "4 Weeks": {
      "Vegetarian diet": polaMakanDash4Minggu,
      "Dash diet": polaMakanDash4Minggu,
      "Mind diet": polaMakanMind4Minggu,
      "Mediterranean diet": polaMakanMediterania4Minggu
    }
  },
];

Map<String, dynamic> textMakan = {
  "1": "textMakan1",
  "2": "textMakan2",
  "3": "textMakan3"
};
Map<String, dynamic> jamMakan = {
  "1": "07:00",
  "2": "13:00",
  "3": "19:00"
};

Map<String, dynamic> polaMakanVeg2Minggu = {
  "1": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_1",
      "2": "pola_makan_v_s_1",
      "3": "pola_makan_v_m_1"
    }
  },
  "2": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_2",
      "2": "pola_makan_v_s_2",
      "3": "pola_makan_v_m_2"
    }
  },
  "3": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_3",
      "2": "pola_makan_v_s_3",
      "3": "pola_makan_v_m_3"
    }
  },
  "4": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_4",
      "2": "pola_makan_v_s_4",
      "3": "pola_makan_v_m_4"
    }
  },
  "5": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_5",
      "2": "pola_makan_v_s_5",
      "3": "pola_makan_v_m_5"
    }
  },
  "6": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_6",
      "2": "pola_makan_v_s_6",
      "3": "pola_makan_v_m_6"
    }
  },
  "7": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_7",
      "2": "pola_makan_v_s_7",
      "3": "pola_makan_v_m_7"
    }
  },
  "8": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_8",
      "2": "pola_makan_v_s_8",
      "3": "pola_makan_v_m_8"
    }
  },
  "9": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_9",
      "2": "pola_makan_v_s_9",
      "3": "pola_makan_v_m_9"
    }
  },
  "10": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_10",
      "2": "pola_makan_v_s_10",
      "3": "pola_makan_v_m_10"
    }
  },
  "11": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_11",
      "2": "pola_makan_v_s_11",
      "3": "pola_makan_v_m_11"
    }
  },
  "12": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_12",
      "2": "pola_makan_v_s_12",
      "3": "pola_makan_v_m_12"
    }
  },
  "13": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_13",
      "2": "pola_makan_v_s_13",
      "3": "pola_makan_v_m_13"
    }
  },
  "14": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_14",
      "2": "pola_makan_v_s_14",
      "3": "pola_makan_v_m_14"
    }
  },  
};

Map<String, dynamic> polaMakanVeg4Minggu = {
  ...polaMakanVeg2Minggu,
  "15": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_15",
      "2": "pola_makan_v_s_15",
      "3": "pola_makan_v_m_15"
    }
  },
  "16": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_16",
      "2": "pola_makan_v_s_16",
      "3": "pola_makan_v_m_16"
    }
  },
  "17": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_17",
      "2": "pola_makan_v_s_17",
      "3": "pola_makan_v_m_17"
    }
  },
  "18": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_18",
      "2": "pola_makan_v_s_18",
      "3": "pola_makan_v_m_18"
    }
  },
  "19": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_19",
      "2": "pola_makan_v_s_19",
      "3": "pola_makan_v_m_19"
    }
  },
  "20": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_20",
      "2": "pola_makan_v_s_20",
      "3": "pola_makan_v_m_20"
    }
  },
  "21": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_21",
      "2": "pola_makan_v_s_21",
      "3": "pola_makan_v_m_21"
    }
  },
  "22": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_22",
      "2": "pola_makan_v_s_22",
      "3": "pola_makan_v_m_22"
    }
  },
  "23": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_23",
      "2": "pola_makan_v_s_23",
      "3": "pola_makan_v_m_23"
    }
  },
  "24": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_24",
      "2": "pola_makan_v_s_24",
      "3": "pola_makan_v_m_24"
    }
  },
  "25": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_25",
      "2": "pola_makan_v_s_25",
      "3": "pola_makan_v_m_25"
    }
  },
  "26": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_26",
      "2": "pola_makan_v_s_26",
      "3": "pola_makan_v_m_26"
    }
  },
  "27": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_27",
      "2": "pola_makan_v_s_27",
      "3": "pola_makan_v_m_27"
    }
  },
  "28": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_28",
      "2": "pola_makan_v_s_28",
      "3": "pola_makan_v_m_28"
    }
  },
  "29": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_29",
      "2": "pola_makan_v_s_29",
      "3": "pola_makan_v_m_29"
    }
  },  
  "30": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_v_p_30",
      "2": "pola_makan_v_s_30",
      "3": "pola_makan_v_m_30"
    }
  },    
};

// --------------- POLA MAKAN DASH --------------- //

Map<String, dynamic> polaMakanDash2Minggu = {
  "1": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_1",
      "2": "pola_makan_d_s_1",
      "3": "pola_makan_d_m_1"
    }
  },
  "2": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_2",
      "2": "pola_makan_d_s_2",
      "3": "pola_makan_d_m_2"
    }
  },
  "3": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_3",
      "2": "pola_makan_d_s_3",
      "3": "pola_makan_d_m_3"
    }
  },
  "4": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_4",
      "2": "pola_makan_d_s_4",
      "3": "pola_makan_d_m_4"
    }
  },
  "5": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_5",
      "2": "pola_makan_d_s_5",
      "3": "pola_makan_d_m_5"
    }
  },
  "6": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_6",
      "2": "pola_makan_d_s_6",
      "3": "pola_makan_d_m_6"
    }
  },
  "7": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_7",
      "2": "pola_makan_d_s_7",
      "3": "pola_makan_d_m_7"
    }
  },
  "8": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_8",
      "2": "pola_makan_d_s_8",
      "3": "pola_makan_d_m_8"
    }
  },
  "9": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_9",
      "2": "pola_makan_d_s_9",
      "3": "pola_makan_d_m_9"
    }
  },
  "10": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_10",
      "2": "pola_makan_d_s_10",
      "3": "pola_makan_d_m_10"
    }
  },
  "11": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_11",
      "2": "pola_makan_d_s_11",
      "3": "pola_makan_d_m_11"
    }
  },
  "12": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_12",
      "2": "pola_makan_d_s_12",
      "3": "pola_makan_d_m_12"
    }
  },
  "13": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_13",
      "2": "pola_makan_d_s_13",
      "3": "pola_makan_d_m_13"
    }
  },
  "14": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_14",
      "2": "pola_makan_d_s_14",
      "3": "pola_makan_d_m_14"
    }
  },  
};

Map<String, dynamic> polaMakanDash4Minggu = {
  ...polaMakanVeg2Minggu,
  "15": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_15",
      "2": "pola_makan_d_s_15",
      "3": "pola_makan_d_m_15"
    }
  },
  "16": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_16",
      "2": "pola_makan_d_s_16",
      "3": "pola_makan_d_m_16"
    }
  },
  "17": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_17",
      "2": "pola_makan_d_s_17",
      "3": "pola_makan_d_m_17"
    }
  },
  "18": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_18",
      "2": "pola_makan_d_s_18",
      "3": "pola_makan_d_m_18"
    }
  },
  "19": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_19",
      "2": "pola_makan_d_s_19",
      "3": "pola_makan_d_m_19"
    }
  },
  "20": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_20",
      "2": "pola_makan_d_s_20",
      "3": "pola_makan_d_m_20"
    }
  },
  "21": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_21",
      "2": "pola_makan_d_s_21",
      "3": "pola_makan_d_m_21"
    }
  },
  "22": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_22",
      "2": "pola_makan_d_s_22",
      "3": "pola_makan_d_m_22"
    }
  },
  "23": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_23",
      "2": "pola_makan_d_s_23",
      "3": "pola_makan_d_m_23"
    }
  },
  "24": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_24",
      "2": "pola_makan_d_s_24",
      "3": "pola_makan_d_m_24"
    }
  },
  "25": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_25",
      "2": "pola_makan_d_s_25",
      "3": "pola_makan_d_m_25"
    }
  },
  "26": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_26",
      "2": "pola_makan_d_s_26",
      "3": "pola_makan_d_m_26"
    }
  },
  "27": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_27",
      "2": "pola_makan_d_s_27",
      "3": "pola_makan_d_m_27"
    }
  },
  "28": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_28",
      "2": "pola_makan_d_s_28",
      "3": "pola_makan_d_m_28"
    }
  },
  "29": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_29",
      "2": "pola_makan_d_s_29",
      "3": "pola_makan_d_m_29"
    }
  },  
  "30": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_d_p_30",
      "2": "pola_makan_d_s_30",
      "3": "pola_makan_d_m_30"
    }
  },    
};

// --------------- POLA MAKAN MIND --------------- //

Map<String, dynamic> polaMakanMind2Minggu = {
  "1": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_1",
      "2": "pola_makan_m_s_1",
      "3": "pola_makan_m_m_1"
    }
  },
  "2": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_2",
      "2": "pola_makan_m_s_2",
      "3": "pola_makan_m_m_2"
    }
  },
  "3": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_3",
      "2": "pola_makan_m_s_3",
      "3": "pola_makan_m_m_3"
    }
  },
  "4": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_4",
      "2": "pola_makan_m_s_4",
      "3": "pola_makan_m_m_4"
    }
  },
  "5": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_5",
      "2": "pola_makan_m_s_5",
      "3": "pola_makan_m_m_5"
    }
  },
  "6": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_6",
      "2": "pola_makan_m_s_6",
      "3": "pola_makan_m_m_6"
    }
  },
  "7": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_7",
      "2": "pola_makan_m_s_7",
      "3": "pola_makan_m_m_7"
    }
  },
  "8": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_8",
      "2": "pola_makan_m_s_8",
      "3": "pola_makan_m_m_8"
    }
  },
  "9": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_9",
      "2": "pola_makan_m_s_9",
      "3": "pola_makan_m_m_9"
    }
  },
  "10": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_10",
      "2": "pola_makan_m_s_10",
      "3": "pola_makan_m_m_10"
    }
  },
  "11": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_11",
      "2": "pola_makan_m_s_11",
      "3": "pola_makan_m_m_11"
    }
  },
  "12": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_12",
      "2": "pola_makan_m_s_12",
      "3": "pola_makan_m_m_12"
    }
  },
  "13": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_13",
      "2": "pola_makan_m_s_13",
      "3": "pola_makan_m_m_13"
    }
  },
  "14": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_14",
      "2": "pola_makan_m_s_14",
      "3": "pola_makan_m_m_14"
    }
  },  
};

Map<String, dynamic> polaMakanMind4Minggu = {
  ...polaMakanVeg2Minggu,
  "15": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_15",
      "2": "pola_makan_m_s_15",
      "3": "pola_makan_m_m_15"
    }
  },
  "16": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_16",
      "2": "pola_makan_m_s_16",
      "3": "pola_makan_m_m_16"
    }
  },
  "17": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_17",
      "2": "pola_makan_m_s_17",
      "3": "pola_makan_m_m_17"
    }
  },
  "18": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_18",
      "2": "pola_makan_m_s_18",
      "3": "pola_makan_m_m_18"
    }
  },
  "19": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_19",
      "2": "pola_makan_m_s_19",
      "3": "pola_makan_m_m_19"
    }
  },
  "20": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_20",
      "2": "pola_makan_m_s_20",
      "3": "pola_makan_m_m_20"
    }
  },
  "21": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_21",
      "2": "pola_makan_m_s_21",
      "3": "pola_makan_m_m_21"
    }
  },
  "22": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_22",
      "2": "pola_makan_m_s_22",
      "3": "pola_makan_m_m_22"
    }
  },
  "23": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_23",
      "2": "pola_makan_m_s_23",
      "3": "pola_makan_m_m_23"
    }
  },
  "24": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_24",
      "2": "pola_makan_m_s_24",
      "3": "pola_makan_m_m_24"
    }
  },
  "25": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_25",
      "2": "pola_makan_m_s_25",
      "3": "pola_makan_m_m_25"
    }
  },
  "26": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_26",
      "2": "pola_makan_m_s_26",
      "3": "pola_makan_m_m_26"
    }
  },
  "27": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_27",
      "2": "pola_makan_m_s_27",
      "3": "pola_makan_m_m_27"
    }
  },
  "28": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_28",
      "2": "pola_makan_m_s_28",
      "3": "pola_makan_m_m_28"
    }
  },
  "29": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_29",
      "2": "pola_makan_m_s_29",
      "3": "pola_makan_m_m_29"
    }
  },  
  "30": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_m_p_30",
      "2": "pola_makan_m_s_30",
      "3": "pola_makan_m_m_30"
    }
  },    
};

// --------------- POLA MAKAN MEDITERANIA --------------- //

Map<String, dynamic> polaMakanMediterania2Minggu = {
  "1": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_1",
      "2": "pola_makan_ma_s_1",
      "3": "pola_makan_ma_m_1"
    }
  },
  "2": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_2",
      "2": "pola_makan_ma_s_2",
      "3": "pola_makan_ma_m_2"
    }
  },
  "3": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_3",
      "2": "pola_makan_ma_s_3",
      "3": "pola_makan_ma_m_3"
    }
  },
  "4": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_4",
      "2": "pola_makan_ma_s_4",
      "3": "pola_makan_ma_m_4"
    }
  },
  "5": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_5",
      "2": "pola_makan_ma_s_5",
      "3": "pola_makan_ma_m_5"
    }
  },
  "6": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_6",
      "2": "pola_makan_ma_s_6",
      "3": "pola_makan_ma_m_6"
    }
  },
  "7": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_7",
      "2": "pola_makan_ma_s_7",
      "3": "pola_makan_ma_m_7"
    }
  },
  "8": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_8",
      "2": "pola_makan_ma_s_8",
      "3": "pola_makan_ma_m_8"
    }
  },
  "9": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_9",
      "2": "pola_makan_ma_s_9",
      "3": "pola_makan_ma_m_9"
    }
  },
  "10": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_10",
      "2": "pola_makan_ma_s_10",
      "3": "pola_makan_ma_m_10"
    }
  },
  "11": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_11",
      "2": "pola_makan_ma_s_11",
      "3": "pola_makan_ma_m_11"
    }
  },
  "12": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_12",
      "2": "pola_makan_ma_s_12",
      "3": "pola_makan_ma_m_12"
    }
  },
  "13": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_13",
      "2": "pola_makan_ma_s_13",
      "3": "pola_makan_ma_m_13"
    }
  },
  "14": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_14",
      "2": "pola_makan_ma_s_14",
      "3": "pola_makan_ma_m_14"
    }
  },  
};

Map<String, dynamic> polaMakanMediterania4Minggu = {
  ...polaMakanVeg2Minggu,
  "15": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_15",
      "2": "pola_makan_ma_s_15",
      "3": "pola_makan_ma_m_15"
    }
  },
  "16": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_16",
      "2": "pola_makan_ma_s_16",
      "3": "pola_makan_ma_m_16"
    }
  },
  "17": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_17",
      "2": "pola_makan_ma_s_17",
      "3": "pola_makan_ma_m_17"
    }
  },
  "18": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_18",
      "2": "pola_makan_ma_s_18",
      "3": "pola_makan_ma_m_18"
    }
  },
  "19": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_19",
      "2": "pola_makan_ma_s_19",
      "3": "pola_makan_ma_m_19"
    }
  },
  "20": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_20",
      "2": "pola_makan_ma_s_20",
      "3": "pola_makan_ma_m_20"
    }
  },
  "21": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_21",
      "2": "pola_makan_ma_s_21",
      "3": "pola_makan_ma_m_21"
    }
  },
  "22": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_22",
      "2": "pola_makan_ma_s_22",
      "3": "pola_makan_ma_m_22"
    }
  },
  "23": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_23",
      "2": "pola_makan_ma_s_23",
      "3": "pola_makan_ma_m_23"
    }
  },
  "24": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_24",
      "2": "pola_makan_ma_s_24",
      "3": "pola_makan_ma_m_24"
    }
  },
  "25": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_25",
      "2": "pola_makan_ma_s_25",
      "3": "pola_makan_ma_m_25"
    }
  },
  "26": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_26",
      "2": "pola_makan_ma_s_26",
      "3": "pola_makan_ma_m_26"
    }
  },
  "27": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_27",
      "2": "pola_makan_ma_s_27",
      "3": "pola_makan_ma_m_27"
    }
  },
  "28": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_28",
      "2": "pola_makan_ma_s_28",
      "3": "pola_makan_ma_m_28"
    }
  },
  "29": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_29",
      "2": "pola_makan_ma_s_29",
      "3": "pola_makan_ma_m_29"
    }
  },  
  "30": {
    "textMakan": textMakan,
    "jamMakan": jamMakan,
    "descriptionMakan": {
      "1": "pola_makan_ma_p_30",
      "2": "pola_makan_ma_s_30",
      "3": "pola_makan_ma_m_30"
    }
  },    
};