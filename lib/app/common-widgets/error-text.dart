import 'package:flutter/material.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ErrorText extends StatelessWidget {
  ErrorText({
    required this.textError,
    required this.icon
  });

  final String textError;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        Row(
          children: [
            Icon(icon, size: 15, color: AppTheme.errorColor,),
            SizedBox(width: 5),
            Text(textError, style: TextStyle(color: AppTheme.errorColor),)
          ],
        )
      ],
    );
  }
}