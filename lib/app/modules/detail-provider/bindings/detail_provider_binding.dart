import 'package:get/get.dart';

import '../controllers/detail_provider_controller.dart';

class DetailProviderBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DetailProviderController>(
      () => DetailProviderController(),
    );
  }
}
