import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/controllers/auth_stream_controller.dart';
import 'package:skripsi_project/app/controllers/notification_controller_controller.dart';
import 'package:skripsi_project/app/controllers/permission_controller.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/translation.dart';

import 'app/routes/app_pages.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]
  );
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await GetStorage.init();
  
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final themeController = Get.put(ThemeController());
  final authStreamController = Get.put(AuthStreamController());
  final fitReminderNotificationController = Get.put(NotificationController());
  final permissionController = Get.put(PermissionController());

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: authStreamController.streamAuthStatus,
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.active){
          return Obx(() => GetMaterialApp(
              title: "Application",
              theme: themeController.theme,
              debugShowCheckedModeBanner: false,
              initialRoute: snapshot.data != null ? Routes.HOME : Routes.WELCOME, 
              getPages: AppPages.routes,
              translations: MyTranslation(),
              locale: Locale(themeController.IsLanguage.value ? 'id' : 'en'),
              defaultTransition: Transition.fadeIn,
            ),
          );
        }else {
          return MaterialApp(
            home: Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
      }
    );
  }
}