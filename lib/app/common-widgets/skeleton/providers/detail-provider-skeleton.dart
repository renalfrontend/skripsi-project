import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

class DetailProviderSkeleton extends StatelessWidget {
  const DetailProviderSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final themeController = Get.find<ThemeController>();

    return SingleChildScrollView(
      child: Container(
        width: size.width,
        height: size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                Shimmer.fromColors(
                  baseColor: themeController.isDarkMode.value
                      ? Colors.grey.shade600
                      : Colors.grey.shade200,
                  highlightColor: themeController.isDarkMode.value
                      ? Colors.white60
                      : Colors.white,
                  child: Container(
                    width: size.width,
                    height: 250,
                    color: Colors.white,
                  ),
                ),
                Positioned(
                  bottom: -50,
                  height: 100,
                  child: Container(
                    width: size.width,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Obx(() => Container(
                        width: 100,
                        height: 100,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          borderRadius:BorderRadius.circular(100),
                          color: themeController.isDarkMode.value ? Colors.grey.shade600 : HexColor("#DFE4ED"),
                        ),
                        child: Icon(Icons.home_work, color: Colors.white, size: 50,),
                      ))
                    )
                  ),
                ),
              ],
            ),
            SizedBox(height: 60),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Shimmer.fromColors(
                    baseColor: themeController.isDarkMode.value
                        ? Colors.grey.shade600
                        : Colors.grey.shade200,
                    highlightColor: themeController.isDarkMode.value
                        ? Colors.white60
                        : Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(width: size.width * 0.5, height: 15, color: Colors.white,),
                        SizedBox(height: 10),
                        Container(width: size.width * 0.5, height: 15, color: Colors.white,),
                        SizedBox(height: 10),
                        Container(width: size.width * 0.8, height: 15, color: Colors.white,),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Shimmer.fromColors(
              baseColor: themeController.isDarkMode.value
                ? Colors.grey.shade600
                : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value
                ? Colors.white60
                : Colors.white,
              child: Container(
                width: size.width,
                height: 150,
                color: Colors.white,
              ),
            ),
    
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Shimmer.fromColors(
                    baseColor: themeController.isDarkMode.value
                        ? Colors.grey.shade600
                        : Colors.grey.shade200,
                    highlightColor: themeController.isDarkMode.value
                        ? Colors.white60
                        : Colors.white,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(width: size.width * 0.5, height: 15, color: Colors.white,),
                        SizedBox(height: 10),
                        Container(width: size.width, height: 30, color: Colors.white,),
                        SizedBox(height: 10),
                        Container(width: size.width * 0.4, height: 15, color: Colors.white,),
                        SizedBox(height: 10),
                        Container(width: size.width, height: 20, color: Colors.white,),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}