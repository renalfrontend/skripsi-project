import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

import '../controllers/information_controller.dart';

class InformationView extends GetView<InformationController> {
  const InformationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,      
        title: Text("Information", style: Theme.of(context).textTheme.titleLarge),
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.angle_left, color: themeController.isDarkMode.value ? Colors.white70 : Colors.black87,),
          onPressed: () => Get.back(),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.only(top: 15),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Image(
                  width: 200,
                  height: 200,
                  image: AssetImage("assets/images/logo.png")
                ),
              ),
              SizedBox(height: 30),
              Text(
                "     ${"information1".tr}",
                style: TextStyle(
                  fontSize: 15,
                  height: 1.5,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "     ${"information2".tr}",
                style: TextStyle(
                  fontSize: 15,
                  height: 1.5,
                ),
              ),
              SizedBox(height: 20),
              Text(
                "     ${"information3".tr}",
                style: TextStyle(
                  fontSize: 15,
                  height: 1.5,
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
