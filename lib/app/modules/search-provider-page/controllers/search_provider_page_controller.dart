import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/services/providers.dart';
import 'package:skripsi_project/constants.dart';

class SearchProviderPageController extends GetxController with StateMixin<Map<String, dynamic>> {
  final storage = GetStorage();
  late TextEditingController searchInput;
  RxString location = ''.obs;
  RxString totalProviders = ''.obs;

  void getAllProviders({String? search="", int? limit=479, String? type}){
    Map<String, dynamic> data;
    change(null, status: RxStatus.loading());
    double latitude = storage.read("latitude") != "" ? storage.read("latitude") : defaultLatitude;
    double longtitude = storage.read("longtitude") != "" ? storage.read("longtitude") : defaultLongtitude;

    Providers()
    .getProviders(
      coordinate: [latitude, longtitude],
      search: search,
      limit: limit.toString()
    )
    .then((value) => {
      data = json.decode(value)['data']['data'],
      
      change(data, status: RxStatus.success()),

      if(type != 'search'){
        location.value = data['data'][0]['location']['location_gmaps'],
      },
      
      totalProviders.value = data['total'].toString(),
    }).catchError((onError) {
      print(onError);
      totalProviders.value = "0";
      change(null, status: RxStatus.error());
    });
  }

  @override
  void onInit() {
    searchInput = TextEditingController(text: "");
    getAllProviders();
    super.onInit();
  }

  @override
  void dispose() {
    searchInput.dispose();
    super.dispose();
  }
}