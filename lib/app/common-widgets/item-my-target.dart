import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/modules/history/controllers/history_controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemMyTarget extends StatelessWidget {
  ItemMyTarget({
    super.key,
    required this.titleTarget,
    required this.desc1,
    required this.desc2,
    required this.color,
    this.type,
    this.id
  });

  String titleTarget;
  String desc1;
  String desc2;
  String color;
  String? type="";
  String? id;

  @override
  Widget build(BuildContext context) {
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    final themeController = Get.find<ThemeController>();
    HistoryController historyController = Get.put(HistoryController());
    
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(top: 20),
        height: 120,
        child: Material(
          elevation: 10,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: Container(
            width: mediaQueryWidth,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: HexColor('$color')
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        titleTarget,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(                                        
                          fontSize: 15,
                          fontWeight: FontWeight.w700,  
                          color: themeController.isDarkMode.value ? Colors.white : AppTheme.darkBackgroundColor
                        ),
                      ),
                      SizedBox(height: 5),
                      Flexible(
                        child: Text(
                          desc1.tr,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(                                        
                            fontSize: 13,
                            color: themeController.isDarkMode.value ? Colors.white70 : AppTheme.darkBackgroundColor.withOpacity(0.8)
                          ),
                        )
                      ),
                      SizedBox(height: 1),
                      Flexible(
                        child: Text(
                          "Target ${desc2.tr}",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 3,
                          style: TextStyle(                                        
                            fontSize: 13,
                            color: themeController.isDarkMode.value ? Colors.white70 : AppTheme.darkBackgroundColor.withOpacity(0.8)
                          ),
                        )
                      ),
                    ],
                  ),
                ),
                Container(
                  child: type != "history" ? null : GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context, 
                        builder: (context){
                          return AlertDialog(
                            title: Text("confirm_popup_title".tr),
                            content: Text("history_popup_desc_backdata".tr),
                            actions: [
                              OutlinedButton(
                                onPressed: () {
                                  Navigator.of(context).pop(false);
                                },
                                style: OutlinedButton.styleFrom(
                                  foregroundColor: AppTheme.primaryColorDark
                                ),
                                child: Text("confirm_popup_btn_no".tr, style: TextStyle(color: AppTheme.primaryColorDark),)                                      
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: AppTheme.primaryColorDark
                                ),
                                onPressed: () {
                                  historyController.backDataTarget(id);

                                  Navigator.of(context).pop(true);
                                }, 
                                child: Text("confirm_popup_btn_yes".tr)
                              )
                            ],
                          );
                        }
                      );
                    },
                    child: Icon(Icons.history),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}