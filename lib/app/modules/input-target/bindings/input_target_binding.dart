import 'package:get/get.dart';

import '../controllers/input_target_controller.dart';

class InputTargetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InputTargetController>(
      () => InputTargetController(),
    );
  }
}
