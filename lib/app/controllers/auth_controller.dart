import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:skripsi_project/app/modules/forgot-password/controllers/forgot_password_controller.dart';
import 'package:skripsi_project/app/modules/login/controllers/login_controller.dart';
import 'package:skripsi_project/app/modules/signup/controllers/signup_controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';
import 'package:skripsi_project/app/utils/firestore.dart';

class AuthController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    clientId: GetPlatform.isIOS ? "481116485687-n68v905u7rv7a1s9qa43qq85j8fkbpe7.apps.googleusercontent.com" : ""
  );
  final storage = GetStorage();

  // Login
  void login(String email, String password) async {
    final loginController = Get.put(LoginController());

    try {
      if(email != "" || password != ""){
        loginController.loginLoading.value = true;
        // login dengan email dan password
        UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email,
          password: password
        );
        
        // ambil data dari collection users dengan id auth
        dynamic docUser = await firestore.collection("users").doc(userCredential.user?.uid).get();

        storage.write('id-user', docUser.data()?['id']);    
        storage.write('login-provider', "");    
        storage.write('createAtUser', await docUser.data()?['createAt']);    

        // Update password user
        await firestore.collection("users").doc(userCredential.user?.uid).update({
          "password": password
        });

        await activeNotificationUser();

        Get.offAllNamed(Routes.HOME);

        loginController.emailError.value = "";        
        loginController.passwordError.value = "";
        loginController.loginLoading.value = false;        
      }else {
        loginController.emailError.value = "Email harus diisi";
        loginController.passwordError.value = "Password harus diisi";
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Get.defaultDialog(
          title: "",
          titlePadding: EdgeInsets.all(0),
          middleText: "No user found for that email."
        );
      } else if (e.code == 'wrong-password') {
        Get.defaultDialog(
          title: "",
          titlePadding: EdgeInsets.all(0),
          middleText: "Wrong password provided for that user."
        );
      }
      loginController.emailError.value = "";        
      loginController.passwordError.value = "";
      loginController.loginLoading.value = false;
    } catch(e){
        print(e);
        Get.defaultDialog(
          title: "Error",
          middleText: "errorTextDescServerGlobal".tr
        );
        loginController.emailError.value = "";        
        loginController.passwordError.value = "";
        loginController.loginLoading.value = false;
      }
  }

  // Login with google
  void loginWithGoogle() async{
    final signupController = Get.put(SignupController());
    final loginController = Get.put(LoginController());

    try{
      GoogleSignInAccount? myAccount = await _googleSignIn.signIn();
      dynamic getUser = await firestore.collection('users').doc(myAccount?.id).get();
      loginController.loginLoading.value = true;
      
      if(myAccount != null){
        final GoogleSignInAuthentication? googleAuth = await myAccount.authentication;

        // Create a new credential
        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth?.accessToken,
          idToken: googleAuth?.idToken,
        );

        // Once signed in, return the UserCredential
        await FirebaseAuth.instance.signInWithCredential(credential);

        firestore.collection("users").doc(myAccount.id).set({
          "id": myAccount.id,
          "fullname": myAccount.displayName,
          "email": myAccount.email,
          "photo": myAccount.photoUrl,
          "password": "",
          "providerLogin": "google",
          "createAt": getUser?.data()?['id'] == myAccount.id ? getUser?.data()['createAt'] : DateTime.now()
        });


        await storage.write('id-user', myAccount.id);
        storage.write('login-provider', "google");
        storage.write('createAtUser', await getUser.data()?['id'] == myAccount.id ? getUser.data()['createAt'] : DateTime.now());    
        loginController.loginLoading.value = false;

        await activeNotificationUser();

        Get.offAllNamed(Routes.HOME);

        signupController.emailError.value = "";
        signupController.fullnameError.value = "";
        signupController.passwordError.value = "";
        loginController.emailError.value = "";        
        loginController.passwordError.value = "";
      }else { 
        loginController.loginLoading.value = false;
        throw "Belum memilih akun google";        
      }
    }catch(error){
      Get.defaultDialog(
        title: "errorTextGlobal".tr,
        middleText: "${error.toString()}"
      );
      print(error.toString());
    }
  }

  // SignUp
  void signup(String fullname, String email, String password) async {
    final signupController = Get.put(SignupController());

    try {      
      if(fullname != "" || email != "" || password != ""){
        signupController.signupLoading.value = true;
        // buat data baru untuk login di auth dengan data user
        UserCredential userAuth = await auth.createUserWithEmailAndPassword(email: email, password: password);

        // buat data baru ke collection users
        await firestore.collection("users").doc(userAuth.user!.uid).set({
          "id": userAuth.user!.uid,
          "fullname": fullname,
          "email": email,
          "password": password,
          "photo": "",
          "providerLogin": "",
          "createAt": DateTime.now()
        });
    
        await storage.write('id-user', userAuth.user?.uid);
        storage.write('login-provider', "");
        storage.write("createAtUser", DateTime.now());
        
        Get.offAllNamed(Routes.HOME);

        signupController.emailError.value = "";
        signupController.fullnameError.value = "";
        signupController.passwordError.value = "";
        signupController.signupLoading.value = false;
      }else {
        signupController.emailError.value = "Email harus diisi";
        signupController.fullnameError.value = "Nama lengkap harus diisi";
        signupController.passwordError.value = "Password harus diisi";
      }
    } catch (e) {
     Get.defaultDialog(
        title: "errorTextGlobal".tr,
        middleText: e.toString()
      );
      signupController.emailError.value = "";
      signupController.fullnameError.value = "";
      signupController.passwordError.value = "";
      signupController.signupLoading.value = false; 
    }
  }

  // Reset password
  void resetPassword(String email) async {
    final forgotPasswordController = Get.put(ForgotPasswordController());

    if(email != "" && GetUtils.isEmail(email)){
      forgotPasswordController.loadingForgotPassword.value = true;

      try{
        await auth.sendPasswordResetEmail(email: email);
        Get.defaultDialog(
          title: "forgetP_titleDialog".tr,
          confirmTextColor: Colors.white,
          buttonColor: AppTheme.primaryColorDark,
          middleText: "${"forgetP_descDialog".tr} $email",
          contentPadding: EdgeInsets.all(15),
          onConfirm: (){
            Get.back(); // close dialog
            Get.back(); // go to login
          },
          textConfirm: "forgetP_btbDialog".tr
        );
        forgotPasswordController.loadingForgotPassword.value = false;
        forgotPasswordController.emailError.value = "";
      } on FirebaseAuthException {
        Get.defaultDialog(
          title: "forgetP_titleError".tr,
          middleText: "$email" + " " + "forgetP_titleDesc".tr
        );
        forgotPasswordController.emailError.value = "";
        forgotPasswordController.loadingForgotPassword.value = false;
      }
    }else {
      forgotPasswordController.emailError.value = "Email harus diisi";
    }
  }

  void logout() async {
    await firestore.collection('users').doc(storage.read('id-user')).collection('pola-list').get().then((value) {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> listData = value.docs;

      if(listData.isNotEmpty){
        listData.forEach((result) {
          dynamic data = result.data();
          for (var i = 0; i < data?['targetNotification']; i++) {
            AwesomeNotifications().cancel(int.parse('${data?['uniqIdForNotification']}${DateTime.now().add(Duration(days: i)).day}'));
          }

          firestore
          .collection('users')
          .doc(storage.read('id-user'))
          .collection('pola-list')
          .doc(data?['id'])
          .update({
            "dayRemoveByUser": DateTime.now().day
          });
        });
      }
      
    });

    await FirebaseAuth.instance.signOut();
    storage.write('login-provider', "");
    Get.offAllNamed(Routes.LOGIN);
  }
}

Future<void> activeNotificationUser() async {
  List<QueryDocumentSnapshot<Map<String, dynamic>>> dataPolaUser = [];
  String userName = "";
  DateTime dateTime = DateTime.now();

  await getUserName().then((value) {
    final data = value.data();
    userName = (data?['fullname'] as String).split(' ')[0];
  });
  
  await firestore
    .collection('users')
    .doc(GetStorage().read('id-user'))
    .collection('pola-list')
    .get()
    .then((value) {
      dataPolaUser = value.docs;
    });

  if (dataPolaUser.isNotEmpty) {  
    dataPolaUser.forEach((result) {
      dynamic data = result.data();

      if(
        data?['targetCompleted'].toString().tr == "Belum Selesai" ||
        data?['targetCompleted'].toString().tr == 'Target not finished'
      ){
        for(var i = 0; i < data?['targetNotification']; i++){
          print("${data?['targetPola'].toString().tr.split(' ')[1]} hari ke ${data?['indexNotificationActive'] + 1}");
          AwesomeNotifications().createNotification(
            content: NotificationContent(
              id: int.parse('${data?['uniqIdForNotification']}${DateTime.now().add(Duration(days: i)).day}'), 
              channelKey: 'fit_reminder_scheduled',
              summary: data?['targetName'],
              title: "${data?['targetPola'].toString().tr.split(' ')[1]} hari ke ${data?['indexNotificationActive'] + 1}",
              body: "Hello ${userName}, sudah waktunya jadwal ${data?['targetPola'].toLowerCase().toString().tr.split(' ')[1]} kamu hari ini. Semoga kamu bisa konsisten :)",
              notificationLayout: NotificationLayout.BigText,
              payload: {
                "nameRoute": "/detail-mytarget/${data?['id'].toString().replaceAll(" ", "")}",
                "arguments": "${data?['colorTarget']}",
                "targetNotification": "${data?['targetNotification']}",
                "idUser": "${GetStorage().read('id-user')}",
                "idPolaUser": "${data?['id'].toString().replaceAll(" ", "")}",
                "indexNotificationActive": "${data?['indexNotificationActive'] + 1}",
                "tabBarIndex": "${data?['tabBarIndex']}",
                "type": "from-login",
              },
            ),
            schedule: NotificationCalendar(
              timeZone: AwesomeNotifications.localTimeZoneIdentifier,
              second: 0,
              millisecond: 0,
              day: 3,
              hour: 21,
              minute: 54
            )
          );
        }
      }
  
      GetStorage().write("tabBarLength", data?['polaDetailTarget']);
    });
  }
}