import 'package:get/get.dart';

import '../controllers/confirmation_target_controller.dart';

class ConfirmationTargetBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ConfirmationTargetController>(
      () => ConfirmationTargetController(),
    );
  }
}
