import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
class ProfileController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  var storage = GetStorage();

  Stream<DocumentSnapshot<Map<String, dynamic>>> streamProfile() {
    return firestore
      .collection("users")
      .doc(storage.read('id-user'))
      .snapshots();
  }

  @override
  void onInit() {
    super.onInit();
  }
}
