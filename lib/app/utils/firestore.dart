import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get_storage/get_storage.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

Future<DocumentSnapshot<Map<String, dynamic>>> getUserName() async {
    return await firestore.collection('users').doc(GetStorage().read('id-user')).get();
  }