import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/item-day-target.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/confirmation_target_controller.dart';

class ConfirmationTargetView extends GetView<ConfirmationTargetController> {
  const ConfirmationTargetView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    Size size = MediaQuery.of(context).size;
    String targetPola = controller.targetPola.value.tr.toLowerCase().split(" ").join('-');

    return Scaffold(
      bottomNavigationBar: Obx(() => BottomAppBar(
        padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
        color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
        child: Container(
          width: size.width,
          height: 40,
          child: ElevatedButton(
            onPressed: () => {
              controller.createTarget(),
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: AppTheme.primaryColorDark
            ),
            child: Text("confirmation_target_button".tr),
          ),
        ),
      )),      
      body: Container(
          child: Obx(() => Container(
          width: MediaQuery.of(context).size.width,  
          height: size.height,
          color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : AppTheme.backgroundColor,
          child: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 40),
            child: Column(
              children: [
                // Header 
                Container(
                  child: Stack(
                    children: [
                      Container(
                        height: size.height * 0.32,
                        child: Image(
                          fit: BoxFit.cover,
                          image: controller.targetPola.value.tr == "Pola Makan" || controller.targetPola.value.tr == "Dietary habit" ? 
                            AssetImage('assets/images/pola-makan/${controller.targetPolaDetail.value.tr.toLowerCase().split(' ').join('-')}.jpeg'):
                            AssetImage('assets/images/pola-olahraga/${controller.targetPolaDetail.value.tr.toLowerCase().split(' ').join('-')}.jpeg')
                        ),
                      ),
                      Positioned(
                        top: 45,
                        child: GestureDetector(
                          onTap: () => Get.back(),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.only(left: 15),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: themeController.isDarkMode.value ? Colors.black45 : Colors.white54
                            ),
                            child: Row(
                              children: [
                                Icon(Icons.arrow_back_ios, size: 16,),
                                Text("Back", style: TextStyle(fontSize: 16),)
                              ],
                            ),
                          ),
                        )
                      ),
                    ],
                  )
                ),

                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text("${'confirmation_target_title1'.tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Text(controller.targetName.value.tr)
                        ]
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text("${'confirmation_target_title2'.tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Text(controller.targetPola.value.tr)
                        ]
                      ),
                      SizedBox(height: 5),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("${controller.targetPola.value.tr == "Pola Makan" || controller.targetPola.value.tr == "Dietary habit" ? "${'confirmation_target_title3_m'.tr}:" : "${'confirmation_target_title3_o'.tr}:"} ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Expanded(child: Text(controller.targetPolaDetail.value.tr))
                        ]
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text("${'confirmation_target_title4'.tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Text(controller.targetWaktu.value.tr)
                        ]
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text("${"createTargetDateText".tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Row(
                            children: [
                              Text("${controller.createDateTarget.value.day}-"),
                              Text("${controller.createDateTarget.value.month}-"),
                              Text("${controller.createDateTarget.value.year}")
                            ],
                          )
                        ]
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Text("${"endTargetDateText".tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                          Row(
                            children: [
                              Text("${controller.endDateTarget.value.day}-"),
                              Text("${controller.endDateTarget.value.month}-"),
                              Text("${controller.endDateTarget.value.year}")
                            ],
                          )
                        ]
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${'confirmation_target_title5'.tr} ${controller.targetPolaDetail.value.tr}", style: TextStyle(fontWeight: FontWeight.bold)),
                            SizedBox(height: 5),
                            Text(controller.polaDescription.value.tr,
                              textAlign: TextAlign.left,
                            )
                          ],
                        ),      
                      ),
                      
                      Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 25),
                            child: Text(
                              "${controller.targetPola.value.tr == "Pola Makan" || controller.targetPola.value.tr == "Dietary habit" ? 'confirmation_target_title6_m'.tr : 'confirmation_target_title6_o'.tr} ", 
                              style: TextStyle(fontWeight: FontWeight.bold)
                            ),
                          ),
                          
                          ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemCount: controller.polaDetailTarget.length,
                            itemBuilder:(context, index) {
                              if(targetPola == 'pola-makan' || targetPola == 'dietary-habit'){
                                return ItemDayTarget(
                                  type: targetPola,
                                  day: index+1,
                                  textMakan1: controller.polaDetailTarget[(index+1).toString()]["textMakan"]["1"],
                                  textMakan2: controller.polaDetailTarget[(index+1).toString()]["textMakan"]["2"],
                                  textMakan3: controller.polaDetailTarget[(index+1).toString()]["textMakan"]["3"],
                                  textMakanTime1: controller.polaDetailTarget[(index+1).toString()]["jamMakan"]["1"],
                                  textMakanTime2: controller.polaDetailTarget[(index+1).toString()]["jamMakan"]["2"],
                                  textMakanTime3: controller.polaDetailTarget[(index+1).toString()]["jamMakan"]["3"],
                                  descMakan1: controller.polaDetailTarget[(index+1).toString()]["descriptionMakan"]["1"],  
                                  descMakan2: controller.polaDetailTarget[(index+1).toString()]["descriptionMakan"]["2"],  
                                  descMakan3: controller.polaDetailTarget[(index+1).toString()]["descriptionMakan"]["3"],  
                                );
                              }else {
                                return ItemDayTarget(
                                  type: targetPola,
                                  day: index+1,
                                  textOlahraga1: controller.polaDetailTarget[(index+1).toString()], 
                                );
                              }
                            },
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ]
            )
          ),
        )
      ))
    );
  }
}
