import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FormHeaderWidget extends StatelessWidget {
  const FormHeaderWidget({
    super.key,
    required this.image,
    required this.title,
    required this.subTitle,
    this.heightBox = 0,
    this.crossAxisAlignment = CrossAxisAlignment.start,
    this.textAlign
  });

  final String image, title, subTitle;
  final double heightBox;
  final CrossAxisAlignment crossAxisAlignment;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      children: [
        SvgPicture.asset(
          image,
          height: MediaQuery.of(context).size.height * 0.16,
          fit: BoxFit.cover,
        ),
        SizedBox(height: heightBox),
        Align(
          alignment: Alignment.center, 
          child: Text(
            title, 
            textAlign: textAlign,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold
            ),
          )
        ),
        SizedBox(height: 5),
        Text(subTitle, textAlign: textAlign, style: Theme.of(context).textTheme.bodyMedium,),
      ],
    );
  }
}