import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/item-my-target.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/search_mytarget_page_controller.dart';

class SearchMytargetPageView extends GetView<SearchMytargetPageController> {
  const SearchMytargetPageView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,      
        title: Text("search_target_page_title".tr, style: Theme.of(context).textTheme.titleLarge),
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.angle_left, color: themeController.isDarkMode.value ? Colors.white70 : Colors.black87,),
          onPressed: () => Get.toNamed(Routes.HOME),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
      ),
      floatingActionButton: CircleAvatar(
        backgroundColor: AppTheme.primaryColorDark,
        child: IconButton(
          onPressed: () => Get.toNamed(Routes.INPUT_TARGET),
          color: Colors.white,
          icon: Icon(Icons.add),
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: [                            
              Expanded(
                child: Obx(() => Container(
                  width: size.width,
                  color: themeController.isDarkMode.value ? HexColor("#0e122f") : AppTheme.backgroundColor,
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                        stream: controller.streamTargets(),
                        builder: (context, snapshot) {
                          final data = snapshot.data?.docs;
                          
                          if(snapshot.connectionState == ConnectionState.active){
                            if(data?.length != 0){                              
                              return ListView.builder(
                                itemCount: data?.length,
                                itemBuilder: (context, index) {
                                  var dataTargets = data![index].data();

                                  return Dismissible(
                                    key: Key(dataTargets['id']),
                                    direction: DismissDirection.endToStart,
                                    background: Container(
                                      alignment: Alignment.centerRight,
                                      padding: EdgeInsets.only(right: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.redAccent,
                                        borderRadius: BorderRadius.all(Radius.circular(20)),
                                      ),
                                      child: Icon(Icons.delete),
                                    ),
                                    confirmDismiss: (direction){
                                      return showDialog(
                                        context: context, 
                                        builder: (context) {
                                          return AlertDialog(
                                            title: Text("confirm_popup_title".tr),
                                            content: Text("confirm_popup_desc".tr),
                                            actions: [
                                              OutlinedButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop(false);
                                                },
                                                style: OutlinedButton.styleFrom(
                                                  foregroundColor: AppTheme.primaryColorDark
                                                ),
                                                child: Text("confirm_popup_btn_no".tr, style: TextStyle(color: AppTheme.primaryColorDark),)                                      
                                              ),
                                              ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                  backgroundColor: AppTheme.primaryColorDark
                                                ),
                                                onPressed: () {
                                                  controller.deleteTargetToHistory(
                                                    dataTargets['id'], 
                                                    dataTargets['uniqIdForNotification'],
                                                    dataTargets['targetPola'], 
                                                    dataTargets['targetName'], 
                                                    dataTargets['targetPolaDetail'], 
                                                    dataTargets['targetWaktu'], 
                                                    dataTargets['polaDescription'], 
                                                    dataTargets['polaDetailTarget'], 
                                                    dataTargets['colorTarget'],
                                                    dataTargets['targetCompleted'],
                                                    dataTargets['targetNotification'],
                                                    dataTargets['indexNotificationActive'],
                                                    dataTargets['tabBarIndex'],
                                                    dataTargets['createDateTarget'].toDate(),
                                                    dataTargets['endDateTarget'].toDate(),
                                                  );

                                                  Navigator.of(context).pop(true);
                                                }, 
                                                child: Text("confirm_popup_btn_yes".tr)
                                              )
                                            ],
                                          );
                                        }
                                      );
                                    },
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.toNamed('/detail-mytarget/${dataTargets['id']}', arguments: dataTargets['colorTarget']);
                                        GetStorage().write("tabBarLength", dataTargets['polaDetailTarget']);
                                        GetStorage().write('tabBarIndex', dataTargets['tabBarIndex']);
                                      },
                                      child: ItemMyTarget(
                                        titleTarget: dataTargets['targetName'],
                                        desc1: dataTargets['targetPolaDetail'],
                                        desc2: dataTargets['targetWaktu'],
                                        color: dataTargets['colorTarget'],
                                      ),
                                    )
                                  );
                                },
                              );
                            }else {
                              return Text("search_target_page_empty".tr); 
                            }
                          }
                    
                          return SizedBox(
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator(color: AppTheme.primaryColorDark),
                          );
                        },
                      ),
                    ),
                  ),             
                )),
              )
            ],
          ),
        ),
      ),
    );
 
  }
}
