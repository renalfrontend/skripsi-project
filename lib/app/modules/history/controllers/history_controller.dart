import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/utils/firestore.dart';

class HistoryController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxString idUser = "".obs;
  GetStorage storage = GetStorage();

  Stream<QuerySnapshot<Map<String, dynamic>>> streamHistory() {
    idUser.value = GetStorage().read('id-user');

    return firestore
      .collection('users')
      .doc(idUser.value)
      .collection('history')
      .orderBy('id', descending: true)
      .snapshots();
  }

  void backDataTarget(dynamic id) async {
    String username = "";
    DateTime dateNow = DateTime.now();

    final dataHistory = await firestore
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('history')
      .doc(id)
      .get();
    dynamic data = dataHistory.data();
  
    await firestore
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('history')
      .doc(id)
      .delete();

    await firestore 
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('pola-list')
      .doc(id)
      .set({
        'id': id,
        "uniqIdForNotification": data?['uniqIdForNotification'],
        "tag-id": data?['tag-id'],
        "targetName": data?['targetName'],
        "targetPola": data?['targetPola'],
        "targetPolaDetail": data?['targetPolaDetail'],
        "targetWaktu": data?['targetWaktu'],
        "polaDescription": data?['polaDescription'],
        "polaDetailTarget": data?['polaDetailTarget'],
        'colorTarget': data?['colorTarget'],
        "targetCompleted": data?['targetCompleted'],
        "targetNotification": data?['targetNotification'],
        "indexNotificationActive": data?['indexNotificationActive'],
        "tabBarIndex": data?['tabBarIndex'],
        "createDateTarget": data?['createDateTarget'],
        "endDateTarget": data?['endDateTarget'],
      });

    await getUserName().then((value) {
      final data = value.data();
      username = (data?['fullname'] as String).split(' ')[0];
    });

    if(
      data?['targetCompleted'].toString().tr == "Belum Selesai" ||
      data?['targetCompleted'].toString().tr == 'Target not finished'
    ){
      // Back and active notification history
      for (var i = 0; i < data?['targetNotification']; i++) {
        AwesomeNotifications().createNotification(
          content: NotificationContent(
            id: int.parse('${data?['uniqIdForNotification']}${DateTime.utc(dateNow.year, dateNow.month, int.parse("${data?['dayRemoveByUser']}")).add(Duration(days: i + 1)).day}'), 
            channelKey: 'fit_reminder_scheduled',
            summary: data?['targetName'],
            title: "${data?['targetPola'].toString().tr.split(' ')[1]} hari ke ${data?['indexNotificationActive'] + 1}",
            body: "Hello ${username}, sudah waktunya jadwal ${data?['targetPola'].toString().tr.split(' ')[1]} kamu hari ini. Semoga kamu bisa konsisten yah :)",
            notificationLayout: NotificationLayout.BigText,
            payload: {
              "nameRoute": "/detail-mytarget/${id}",
              "arguments": "${data?['colorTarget']}",
              "targetNotification": "${data?['targetNotification']}",
              "idUser": "${GetStorage().read('id-user')}",
              "idPolaUser": "${data?['id']}",
              "dayRemoveByUser": "${data?['dayRemoveByUser']}",
              "indexNotificationActive": "${data?['indexNotificationActive'] + 1}",
              "type": "from-history",
            },
          ),

          schedule: NotificationCalendar(
            timeZone: AwesomeNotifications.localTimeZoneIdentifier,
            second: 0,
            millisecond: 0,
            day: DateTime.utc(dateNow.year, dateNow.month, int.parse("${data?['dayRemoveByUser']}")).add(Duration(days: i + 1)).day,
            hour: 07,
            minute: 00
          )
        );
      }
    }
    
    GetStorage().write("tabBarLength", data?['polaDetailTarget']);
  }

  void deleteHistory(dynamic id) async {
    await firestore
      .collection('users')
      .doc(storage.read('id-user'))
      .collection('history')
      .doc(id)
      .delete();
  }
}
