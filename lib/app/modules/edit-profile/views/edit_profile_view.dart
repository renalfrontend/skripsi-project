import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/input-form.dart';
import 'package:skripsi_project/app/common-widgets/loading-screen.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/edit_profile_controller.dart';

class EditProfileView extends GetView<EditProfileController> {
  const EditProfileView({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final loginProvider = GetStorage().read('login-provider');
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    final myAppBar = AppBar(
      shadowColor: Colors.transparent,      
      title: Text("ubahP_title".tr, style: Theme.of(context).textTheme.titleLarge),
      leading: IconButton(
        icon: Icon(LineAwesomeIcons.angle_left, color: themeController.isDarkMode.value ? Colors.white70 : Colors.black87,),
        onPressed: () => Get.back(),
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
      ),
    );
    final bodyHeight = mediaQueryHeight - myAppBar.preferredSize.height - MediaQuery.of(context).padding.top;
    List months = ['january','february','march','april','may','june','july','august','september','october','november','december'];

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: myAppBar,
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.all(30),
                height: bodyHeight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Align(
                          alignment: Alignment.center,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              SizedBox(
                                width: 120,
                                height: 120,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Obx(() => 
                                    CachedNetworkImage(
                                      fit: BoxFit.cover,
                                      imageUrl: controller.imageUrl.value == "" ? "https://icones.pro/wp-content/uploads/2021/05/symbole-de-l-homme-violet.png" : controller.imageUrl.value,
                                      progressIndicatorBuilder: (context, url, downloadProgress) => CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark),
                                      errorWidget: (context, url, error) => Image.network("https://cdn-icons-png.flaticon.com/512/42/42994.png"),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: loginProvider != 'google' ? 
                                Positioned(
                                bottom: 0,
                                right: 0,
                                child: GestureDetector(
                                  onTap: () => controller.uploadFile(),
                                  child: Container(
                                    width: 35,
                                    height: 35,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: AppTheme.primaryColorDark
                                    ),
                                    child: Icon(
                                      LineAwesomeIcons.camera,
                                    ),
                                  ),
                                ),
                              ) : Container(),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 50),
                        Form(
                          child: Column(
                            children: [
                              InputForm(
                                controller: controller.fullname,
                                label: "ubahP_label1".tr, 
                                placeholder: "ubahP_input1".tr, 
                                icon: Icons.person,
                                loginProvider: loginProvider
                              ),
                              SizedBox(height: 10),
                              InputForm(
                                controller: controller.email,
                                label: "Email", 
                                placeholder: "ubahP_input2".tr, 
                                icon: Icons.email,
                                loginProvider: loginProvider
                              ),
                              SizedBox(height: 10),
                              InputForm(
                                controller: controller.password,
                                label: "Password", 
                                placeholder: "ubahP_input3".tr, 
                                icon: Icons.lock,
                                loginProvider: loginProvider
                              ),
                              SizedBox(height: 20),
                              Container(
                                child: loginProvider == 'google' ? Text("ubahP_providerLogin".tr, style: TextStyle(color: AppTheme.primaryColorDark),) : null,
                              ),
                              Container(
                                child: loginProvider != 'google' ? 
                                SizedBox(
                                width: double.infinity,
                                child: ElevatedButton(
                                  onPressed: () {
                                    controller.updateAccount(
                                      controller.fullname.text,
                                      controller.email.text,
                                      controller.password.text
                                    );
                                  },
                                  child: Text("ubahP_title".tr, style: TextStyle(color: Colors.white),),
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: AppTheme.primaryColorDark, side: BorderSide.none, shape: StadiumBorder()
                                  ),
                                ),
                              ) : Container(),
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        )
                      ]
                    ),
                    StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                      stream: controller.userDetailStream(),
                      builder:(context, snapshot) {
                        dynamic data = snapshot.data?.data();

                        if(snapshot.connectionState == ConnectionState.active){
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("${"ubahP_textF1".tr} ${data?['createAt'].toDate().day} ${months[data?['createAt'].toDate().month-1]} ${data?['createAt'].toDate().year}"),
                              ElevatedButton(
                                onPressed: () => controller.deleteUser(), 
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.redAccent.withOpacity(0.1),
                                  elevation: 0,
                                  foregroundColor: Colors.red,
                                  shape: StadiumBorder(),
                                  side: BorderSide.none
                                ),
                                child: Text("ubahP_btn".tr),
                              )
                            ],
                          );
                        }

                        return Container();
                      }, 
                    )
                  ],
                ),
              ),
              Obx(() => controller.progressUpload >= 100 || controller.progressUpload == 0.0 ? Container() : LoadingScreen(bodyHeight: bodyHeight))
            ],
          ),
        )
      ),
    );
  }
}
