import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SearchMytargetPageController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  RxString idUser = "".obs;

  Stream<QuerySnapshot<Map<String, dynamic>>> streamTargets() {
    idUser.value = GetStorage().read('id-user');

    return firestore
      .collection('users')
      .doc(idUser.value)
      .collection('pola-list')
      .orderBy('id', descending: true)
      .snapshots();
  }
  
  void deleteTargetToHistory(
    String idTarget,
    int uniqIdForNotification,
    String targetPola,
    String targetName,
    String targetPolaDetail,
    String targetWaktu,
    String polaDescription,
    Map<String, dynamic> polaDetailTarget,
    String colorTarget,
    String targetCompleted,
    int targetNotification,
    dynamic indexNotificationActive,
    dynamic tabBarIndex,
    DateTime createDateTarget,
    DateTime endDateTarget
  ) {
    firestore.collection('users').doc(idUser.value).collection('pola-list').doc(idTarget).delete();

    firestore
      .collection('users')
      .doc(idUser.value)
      .collection('history')
      .doc(idTarget.toString())
      .set({
        "id": idTarget,
        "uniqIdForNotification": uniqIdForNotification,
        "tag-id": targetPola.toString().tr.toLowerCase().split(" ").join('-'),
        "targetName": targetName,
        "targetPola": targetPola,
        "targetPolaDetail": targetPolaDetail,
        "targetWaktu": targetWaktu,
        "polaDescription": polaDescription,
        "polaDetailTarget": polaDetailTarget,
        'colorTarget': colorTarget,
        "targetCompleted": targetCompleted,
        "targetNotification": targetNotification,
        "indexNotificationActive": indexNotificationActive,
        "tabBarIndex": tabBarIndex,
        "targetDateRemoveAutomatic": DateTime.now().day + 7,
        "dayRemoveByUser": DateTime.now().day,
        "createDateTarget": createDateTarget,
        "endDateTarget": endDateTarget
      });

    // unsubscribe notification when delete to history
    for(var i = 0; i < targetNotification; i++){
      AwesomeNotifications().cancel(int.parse('${uniqIdForNotification}${DateTime.now().add(Duration(days: i)).day}'));
    }
  }

  @override
  void onInit() {
    super.onInit();
  }
}
