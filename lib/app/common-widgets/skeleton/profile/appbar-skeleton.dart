import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

class AppBarSkeleton extends StatelessWidget implements PreferredSizeWidget{
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    
    return AppBar(
          shadowColor: Colors.transparent,      
          title: Obx(() => Shimmer.fromColors(
            baseColor: themeController.isDarkMode.value ? Colors.grey.shade600 : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value ? Colors.white60 : Colors.white,
            child: Container(
              width: 100,
              height: 5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.amber
              ),
            )
          )),
          leading: Obx(() => IconButton(
            icon: Icon(LineAwesomeIcons.angle_left),
            onPressed: () => null,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            color: themeController.isDarkMode.value ? Colors.white : Colors.black,
          )),
    );    
  }
  
  @override
  Size get preferredSize => const Size.fromHeight(100);
}