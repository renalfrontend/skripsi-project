import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/information-target.dart';
import 'package:skripsi_project/app/common-widgets/item-day-target.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/data/pola-makan.dart';
import 'package:skripsi_project/app/data/pola-olahraga.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/detail_mytarget_controller.dart';

class DetailMytargetView extends GetView<DetailMytargetController> {
  const DetailMytargetView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,      
        title: Text("menu_2".tr, style: Theme.of(context).textTheme.titleLarge?.copyWith(color: Colors.white)),        
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.angle_left, color: Colors.white,),
          onPressed: () => Get.back(),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
        backgroundColor: themeController.isDarkMode.value ? HexColor("#1a2257") : HexColor('${Get.arguments}'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: themeController.isDarkMode.value ? HexColor("#1a2257") : HexColor('${Get.arguments}'),
              padding: EdgeInsets.all(20),
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor.withOpacity(0.7) : Colors.white30,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: TabBar(
                  controller: controller.tabController1,
                  indicatorColor: Colors.transparent,
                  indicatorWeight: 2,
                  indicator: BoxDecoration(
                    color: themeController.isDarkMode.value ? AppTheme.primaryColorDark : Colors.white,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  labelColor: themeController.isDarkMode.value ? Colors.white : HexColor(Get.arguments),
                  tabs: [
                    Tab(text: "detail_target_tabbar_title1".tr,),
                    Tab(text: "detail_target_tabbar_title2".tr,)
                  ],
                ),
              )
            ),

            Expanded(
              child: TabBarView(
                controller: controller.tabController1,                
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : AppTheme.backgroundColor,
                    child: StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                      stream: controller.streamDetailTargets(),
                      builder: (context, snapshot) {
                        if(snapshot.connectionState == ConnectionState.active){
                          final data = snapshot.data?.data();

                          return InformationTarget(
                            namaTarget: data?['targetName'],
                            namaPola: data?['targetPolaDetail'],
                            waktuTarget: data?['targetWaktu'],
                            text: data?['tag-id'] == 'pola-makan' || data?['tag-id'] == 'dietary-habit' ? polaMakanType[0][data?['targetPolaDetail'].toString().tr]['description'] : polaOlahragaType[0][data?['targetPolaDetail'].toString().tr]['description'],
                            statusTarget: data?['targetCompleted'],
                            createDateTarget: data?['createDateTarget'].toDate(),
                            endDateTarget: data?['endDateTarget'].toDate(),
                          );
                        }

                        return Container();
                      },
                    )
                  ),

                  // Tabbar 2
                  Container(                    
                    color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : AppTheme.backgroundColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: double.infinity,
                          height: 80,
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white70,
                          ),
                          child: StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                            stream: controller.streamDetailTargets(),
                            builder: (context, snapshot) {
                              if(snapshot.connectionState == ConnectionState.active){
                                Map<String, dynamic>? data = snapshot.data?.data();
                                
                                return TabBar(
                                  isScrollable: true,
                                  controller: controller.tabController2,
                                  indicatorColor: Colors.transparent,
                                  indicatorWeight: 2,
                                  indicator: BoxDecoration(
                                    color: themeController.isDarkMode.value ? AppTheme.primaryColorDark : Colors.white,
                                    border: Border.all(color: themeController.isDarkMode.value ? AppTheme.primaryColorDark : HexColor(Get.arguments)),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  labelColor: themeController.isDarkMode.value ? Colors.white : HexColor(Get.arguments),
                                  tabs: [
                                    for(var i = 0; i < data?['polaDetailTarget'].length; i++) Tab(
                                      child: Container(
                                        width: 70,
                                        child: Center(
                                          child: Text("${"detail_target_item_title".tr} ${i+1}", textAlign: TextAlign.center,),
                                        ),
                                      ),
                                    )
                                  ]
                                );
                              }

                              return Container();
                            },
                          )
                        ),
                                      
                        Container(
                          child: Expanded(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: StreamBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                                stream: controller.streamDetailTargets(),
                                builder: (context, snapshot) {
                                  if(snapshot.connectionState == ConnectionState.active){
                                    Map<String, dynamic>? data = snapshot.data?.data();

                                    if(data!['tag-id'] == 'pola-makan' || data['tag-id'] == 'dietary-habit'){
                                      return TabBarView(
                                        controller: controller.tabController2,
                                        children: [
                                          for(var i = 0; i < data['polaDetailTarget'].length; i++) ItemDayTarget(
                                            type: data['tag-id'],
                                            uniqIdForNotification: data['uniqIdForNotification'],
                                            textMakan1: data['polaDetailTarget'][(i+1).toString()]['textMakan']["1"],
                                            textMakanTime1: data['polaDetailTarget'][(i+1).toString()]['jamMakan']["1"],
                                            descMakan1: data['polaDetailTarget'][(i+1).toString()]['descriptionMakan']["1"],
                                            textMakan2: data['polaDetailTarget'][(i+1).toString()]['textMakan']["2"],
                                            textMakanTime2: data['polaDetailTarget'][(i+1).toString()]['jamMakan']["2"],
                                            descMakan2: data['polaDetailTarget'][(i+1).toString()]['descriptionMakan']["2"],
                                            textMakan3: data['polaDetailTarget'][(i+1).toString()]['textMakan']["3"],
                                            textMakanTime3: data['polaDetailTarget'][(i+1).toString()]['jamMakan']["3"],
                                            descMakan3: data['polaDetailTarget'][(i+1).toString()]['descriptionMakan']["3"],
                                            numbEndItem: (i+1).toString(),
                                            totalTarget: data['polaDetailTarget'].length,
                                          ),                                        
                                        ],
                                      );
                                    }else {
                                      return TabBarView(
                                        controller: controller.tabController2,
                                        children: [
                                          for(var i = 0; i < data['polaDetailTarget'].length; i++) ItemDayTarget(
                                            type: data['tag-id'],
                                            uniqIdForNotification: data['uniqIdForNotification'],
                                            textOlahraga1: data['polaDetailTarget'][(i+1).toString()],
                                            numbEndItem: (i+1).toString(),
                                            totalTarget: data['polaDetailTarget'].length
                                          ),                                        
                                        ],
                                      );
                                    }
                                  }

                                  return Container();
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
