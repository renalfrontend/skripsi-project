import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:skripsi_project/app/common-widgets/form-header-widget.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/modules/login/widgets/login-footer-widget.dart';
import 'package:skripsi_project/app/modules/login/widgets/login-form-widget.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final authC = Get.put(AuthController());
    
    return SafeArea(
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
         body: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.all(30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FormHeaderWidget(
                      image: "assets/images/login.svg",
                      title: "login_title".tr,
                      heightBox: 25,
                      subTitle: "login_desc".tr,
                    ),
                    LoginFormWidget(),
                    LoginFooterWidget(),
                  ],
                ),
              ),

              Obx(() => controller.loginLoading.value ? Positioned(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.black.withOpacity(0.5),
                  child: Center(
                    child: CircularProgressIndicator(color: AppTheme.primaryColorDark, strokeWidth: 10,),
                  ),
                ),
              ) : Container())              
            ],
          ),
         ),
        ),
      ),
    );
  }
}