import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class InputSelect extends StatelessWidget {
  InputSelect({
    super.key,
    this.items,
    this.label,
    this.inputSelect,
    this.streamInputPola,
    this.onChanged
  });

  List<String>? items;
  String? label;
  RxString? inputSelect="".obs;
  RxBool? streamInputPola = false.obs;
  Function(Object value)? onChanged; 

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    
    return Obx(() => Container(
      color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : streamInputPola?.value == false ? Colors.grey.shade200 : Colors.white,
      child: DropdownButtonFormField2( 
        dropdownButtonKey: this.key,
        value: inputSelect!.value.isEmpty ? null : inputSelect!.value,
        decoration: InputDecoration(
          isDense: true,
          border: OutlineInputBorder(            
            borderRadius: BorderRadius.circular(5),
          ),
          contentPadding: EdgeInsets.symmetric(vertical: 13).copyWith(right: 5),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppTheme.primaryColorDark)
          )
        ),                        
        hint: Text(
          streamInputPola?.value == false ? "" : label!,
          style: TextStyle(fontSize: 14),
        ),
        items: streamInputPola?.value != false ? items!.map((item) => DropdownMenuItem<String>(
          value: item,
          child: Text(
            item.tr,
            style: const TextStyle(
              fontSize: 13,
            ),
          ),
        )).toList() : [],
        onChanged:(value) => onChanged!(value!),
        dropdownStyleData: DropdownStyleData(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
    ));
  }
}