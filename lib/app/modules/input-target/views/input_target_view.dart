import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:skripsi_project/app/common-widgets/error-text.dart';
import 'package:skripsi_project/app/common-widgets/input-form.dart';
import 'package:skripsi_project/app/common-widgets/input-select.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/data/global-data.dart';
import 'package:skripsi_project/app/data/pola-makan.dart';
import 'package:skripsi_project/app/data/pola-olahraga.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/input_target_controller.dart';

class InputTargetView extends GetView<InputTargetController> {
  InputTargetView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final mediaQueryHeight = MediaQuery.of(context).size.height;
    final mediaQueryWidth = MediaQuery.of(context).size.width;
    final bodyHeight = mediaQueryHeight - MediaQuery.of(context).padding.top;

    return Scaffold(
      bottomNavigationBar: Obx(() => BottomAppBar(
        padding: EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
        color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
        child: Container(
          width: mediaQueryWidth,
          height: 40,
          child: ElevatedButton(
            onPressed: () {
              controller.submitTargetForm(
                controller.inputTargetName.text,
                controller.inputPola.value,
                controller.inputPolaMknOrolhrg.value,
                controller.inputWaktuTarget.value,
                controller.color.value
              );
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: AppTheme.primaryColorDark
            ),
            child: Text("Lanjut"),
          ),
        ),
      )),
      body: Container(
          child: Obx(() => Container(
          width: MediaQuery.of(context).size.width,  
          height: bodyHeight,
          color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : AppTheme.backgroundColor,
          child: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 40),
            child: Column(
              children: [
                // Header 
                Container(
                  child: Stack(
                    children: [
                      Container(
                        height: bodyHeight * 0.3,
                        child: Image(
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/bg-input.png"),
                        ),
                      ),
                      Positioned(
                        top: 50,
                        left: 0,
                        child: GestureDetector(
                          onTap: () => Get.back(),
                          child: Row(
                            children: [
                              Icon(Icons.arrow_back_ios, size: 16,),
                              Text("Back", style: TextStyle(fontSize: 16),)
                            ],
                          ),
                        )
                      )
                    ],
                  )
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 30),
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    children: [
                      InputForm(
                        label: "input_target_label".tr, 
                        placeholder: "input_target_placeholder_1".tr,
                        controller: controller.inputTargetName,
                      ),
                      Obx(() => controller.inputTargetNameError.value != "" ? ErrorText(textError: "input_target_error1".tr, icon: Icons.warning_outlined) : Container()),
                      SizedBox(height: 20),
                      InputSelect(
                        key: Key("1"),
                        items: itemsInputPola, 
                        label: "input_target_placeholder_2".tr, 
                        inputSelect: controller.inputPola,
                        onChanged: (value) {
                          controller.fillForm(
                            value, 
                            controller.inputPola,
                            controller.inputPolaError
                          );
                        },
                      ),
                      Obx(() => controller.inputPolaError.value != "" ? ErrorText(textError: "input_target_error2".tr, icon: Icons.warning_outlined) : Container()),
                      SizedBox(height: 20),
                      InputSelect(
                        key: Key("2"),
                        items: controller.inputPola.value.tr == 'Pola Makan' || controller.inputPola.value.tr == "Dietary habit" ? polaMakanType[0]['itemsInputPolaMakan'] : polaOlahragaType[0]['itemsInputPolaOlahraga'], 
                        label: controller.inputPola.value.tr == "Pola Makan" || controller.inputPola.value.tr == "Dietary habit" ? "input_target_placeholder_3_pola_makan".tr : "input_target_placeholder_3_pola_olahraga".tr, 
                        streamInputPola: controller.streamInputPola,
                        inputSelect: controller.inputPolaMknOrolhrg,
                        onChanged: (value) {
                          controller.fillForm(
                            value, 
                            controller.inputPolaMknOrolhrg,
                            controller.inputPolaMknOrolhrgError
                          );
                        },
                      ),
                      Obx(() => controller.inputPolaMknOrolhrgError.value != "" ? ErrorText(textError: "input_target_error3".tr, icon: Icons.warning_outlined) : Container()),
                      SizedBox(height: 20),
                      InputSelect(
                        key: Key("3"),
                        items: itemsWaktuTarget, 
                        label: "input_target_placeholder_4".tr, 
                        streamInputPola: controller.streamInputPola,
                        inputSelect: controller.inputWaktuTarget,
                        onChanged: (value) {
                          controller.fillForm(
                            value, 
                            controller.inputWaktuTarget,
                            controller.inputWaktuTargetError
                          );
                        },
                      ),
                      Obx(() => controller.inputWaktuTargetError.value != "" ? ErrorText(textError: "input_target_error4".tr, icon: Icons.warning_outlined) : Container()),
                      SizedBox(height: 20),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("${"input_target_warna".tr} "),
                          SizedBox(width: 10),
                          Row(
                            children: [
                              for(var i = 0; i < mycolors.length; i++) GestureDetector(
                                onTap: () => controller.changeColor(mycolors[i]),                                
                                child: Container(
                                  width: 30, 
                                  height: 30,
                                  margin: EdgeInsets.only(right: 10), 
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    color: HexColor("${mycolors[i]}"),
                                  ),
                                  child: controller.color.value == mycolors[i] ? Icon(Icons.done, color: Colors.green) : null
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ))
      ),
    );
  }
}
