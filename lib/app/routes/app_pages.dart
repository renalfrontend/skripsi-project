import 'package:get/get.dart';

import '../modules/confirmation-target/bindings/confirmation_target_binding.dart';
import '../modules/confirmation-target/views/confirmation_target_view.dart';
import '../modules/detail-mytarget/bindings/detail_mytarget_binding.dart';
import '../modules/detail-mytarget/views/detail_mytarget_view.dart';
import '../modules/detail-provider/bindings/detail_provider_binding.dart';
import '../modules/detail-provider/views/detail_provider_view.dart';
import '../modules/edit-profile/bindings/edit_profile_binding.dart';
import '../modules/edit-profile/views/edit_profile_view.dart';
import '../modules/forgot-password/bindings/forgot_password_binding.dart';
import '../modules/forgot-password/views/forgot_password_view.dart';
import '../modules/history/bindings/history_binding.dart';
import '../modules/history/views/history_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/information/bindings/information_binding.dart';
import '../modules/information/views/information_view.dart';
import '../modules/input-target/bindings/input_target_binding.dart';
import '../modules/input-target/views/input_target_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/profile/bindings/profile_binding.dart';
import '../modules/profile/views/profile_view.dart';
import '../modules/search-mytarget-page/bindings/search_mytarget_page_binding.dart';
import '../modules/search-mytarget-page/views/search_mytarget_page_view.dart';
import '../modules/search-provider-page/bindings/search_provider_page_binding.dart';
import '../modules/search-provider-page/views/search_provider_page_view.dart';
import '../modules/signup/bindings/signup_binding.dart';
import '../modules/signup/views/signup_view.dart';
import '../modules/welcome/bindings/welcome_binding.dart';
import '../modules/welcome/views/welcome_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.WELCOME,
      page: () => const WelcomeView(),
      binding: WelcomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => const SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASSWORD,
      page: () => const ForgetPasswordView(),
      binding: ForgotPasswordBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => const ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_PROFILE,
      page: () => const EditProfileView(),
      binding: EditProfileBinding(),
    ),
    GetPage(
      name: _Paths.INFORMATION,
      page: () => const InformationView(),
      binding: InformationBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH_PROVIDER_PAGE,
      page: () => SearchProviderPageView(),
      binding: SearchProviderPageBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_PROVIDER,
      page: () => const DetailProviderView(),
      binding: DetailProviderBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH_MYTARGET_PAGE,
      page: () => const SearchMytargetPageView(),
      binding: SearchMytargetPageBinding(),
    ),
    GetPage(
      name: _Paths.INPUT_TARGET,
      page: () => InputTargetView(),
      binding: InputTargetBinding(),
    ),
    GetPage(
      name: _Paths.CONFIRMATION_TARGET,
      page: () => const ConfirmationTargetView(),
      binding: ConfirmationTargetBinding(),
    ),
    GetPage(
      name: _Paths.DETAIL_MYTARGET,
      page: () => const DetailMytargetView(),
      binding: DetailMytargetBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY,
      page: () => const HistoryView(),
      binding: HistoryBinding(),
    ),
  ];
}
