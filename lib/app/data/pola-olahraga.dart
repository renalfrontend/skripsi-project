List<Map<String, dynamic>> polaOlahragaType = [
  {
    "pola": "Pola Olahraga",

    "itemsInputPolaOlahraga": [
      "input_target_pola_olahraga_l",
      "input_target_pola_olahraga_f",
      "input_target_pola_olahraga_s",
      "input_target_pola_olahraga_k"
    ],

    "Pola olahraga luar ruangan": {
      "description": "desc_pola_o_luar"
    },
    "Outdoor sport pattern": {
      "description": "desc_pola_o_luar"
    },
    "Pola olahraga fleksibilitas": {
      "description": "desc_pola_o_flexible"
    },
    "Flexibility outdoor sport pattern": {
      "description": "desc_pola_o_flexible"
    },
    "Pola olahraga senam": {
      "description": "desc_pola_o_senam"
    },
    "Gymnastics outdoor sport pattern": {
      "description": "desc_pola_o_senam"
    },
    "Pola olahraga kardiovaskular": {
      "description": "desc_pola_o_kardiovaskular"
    },
    "Cardiovascular outer sport pattern": {
      "description": "desc_pola_o_kardiovaskular"
    },

    "2 Minggu": {
      "Pola olahraga luar ruangan": polaOlahragaLuarRuangan2Minggu,
      "Pola olahraga fleksibilitas": polaOlahragaFleksible2Minggu,
      "Pola olahraga senam": polaOlahragaSenam2Minggu,
      "Pola olahraga kardiovaskular": polaOlahragaKardiovaskular2Minggu
    },
    "2 Weeks": {
      "Outdoor sport pattern": polaOlahragaLuarRuangan2Minggu,
      "Flexibility outdoor sport pattern": polaOlahragaFleksible2Minggu,
      "Gymnastics outdoor sport pattern": polaOlahragaSenam2Minggu,
      "Cardiovascular outer sport pattern": polaOlahragaKardiovaskular2Minggu
    },
    "4 Minggu": {
      "Pola olahraga luar ruangan": polaOlahragaLuarRuangan4Minggu,
      "Pola olahraga fleksibilitas": polaOlahragaFleksible4Minggu,
      "Pola olahraga senam": polaOlahragaSenam4Minggu,
      "Pola olahraga kardiovaskular": polaOlahragaKardiovaskular4Minggu
    },
    "4 Weeks": {
      "Outdoor sport pattern": polaOlahragaLuarRuangan4Minggu,
      "Flexibility outdoor sport pattern": polaOlahragaFleksible4Minggu,
      "Gymnastics outdoor sport pattern": polaOlahragaSenam4Minggu,
      "Cardiovascular outer sport pattern": polaOlahragaKardiovaskular4Minggu
    }
  }
];

Map<String, dynamic> polaOlahragaLuarRuangan2Minggu = {
  "1": "pola_olahraga_luar_ruangan_1",
  "2": "pola_olahraga_luar_ruangan_2",
  "3": "pola_olahraga_luar_ruangan_3",
  "4": "pola_olahraga_luar_ruangan_4",
  "5": "pola_olahraga_luar_ruangan_5",
  "6": "pola_olahraga_luar_ruangan_6",
  "7": "pola_olahraga_luar_ruangan_7",
  "8": "pola_olahraga_luar_ruangan_8",
  "9": "pola_olahraga_luar_ruangan_9",
  "10": "pola_olahraga_luar_ruangan_10",
  "11": "pola_olahraga_luar_ruangan_11",
  "12": "pola_olahraga_luar_ruangan_12",
  "13": "pola_olahraga_luar_ruangan_13",
  "14": "pola_olahraga_luar_ruangan_14",
};

Map<String, dynamic> polaOlahragaLuarRuangan4Minggu = {
  "1": "pola_olahraga_luar_ruangan_1",
  "2": "pola_olahraga_luar_ruangan_2",
  "3": "pola_olahraga_luar_ruangan_3",
  "4": "pola_olahraga_luar_ruangan_4",
  "5": "pola_olahraga_luar_ruangan_5",
  "6": "pola_olahraga_luar_ruangan_6",
  "7": "pola_olahraga_luar_ruangan_7",
  "8": "pola_olahraga_luar_ruangan_8",
  "9": "pola_olahraga_luar_ruangan_9",
  "10": "pola_olahraga_luar_ruangan_10",
  "11": "pola_olahraga_luar_ruangan_11",
  "12": "pola_olahraga_luar_ruangan_12",
  "13": "pola_olahraga_luar_ruangan_13",
  "14": "pola_olahraga_luar_ruangan_14",
  "15": "pola_olahraga_luar_ruangan_15",
  "16": "pola_olahraga_luar_ruangan_16",
  "17": "pola_olahraga_luar_ruangan_17",
  "18": "pola_olahraga_luar_ruangan_18",
  "19": "pola_olahraga_luar_ruangan_19",
  "20": "pola_olahraga_luar_ruangan_20",
  "21": "pola_olahraga_luar_ruangan_21",
  "22": "pola_olahraga_luar_ruangan_22",
  "23": "pola_olahraga_luar_ruangan_23",
  "24": "pola_olahraga_luar_ruangan_24",
  "25": "pola_olahraga_luar_ruangan_25",
  "26": "pola_olahraga_luar_ruangan_26",
  "27": "pola_olahraga_luar_ruangan_27",
  "28": "pola_olahraga_luar_ruangan_28",
  "29": "pola_olahraga_luar_ruangan_29",
  "30": "pola_olahraga_luar_ruangan_30",
};

Map<String, dynamic> polaOlahragaFleksible2Minggu = {
  "1": "pola_olahraga_fleksibilitas_1",
  "2": "pola_olahraga_fleksibilitas_2",
  "3": "pola_olahraga_fleksibilitas_3",
  "4": "pola_olahraga_fleksibilitas_4",
  "5": "pola_olahraga_fleksibilitas_5",
  "6": "pola_olahraga_fleksibilitas_6",
  "7": "pola_olahraga_fleksibilitas_7",
  "8": "pola_olahraga_fleksibilitas_8",
  "9": "pola_olahraga_fleksibilitas_9",
  "10": "pola_olahraga_fleksibilitas_10",
  "11": "pola_olahraga_fleksibilitas_11",
  "12": "pola_olahraga_fleksibilitas_12",
  "13": "pola_olahraga_fleksibilitas_13",
  "14": "pola_olahraga_fleksibilitas_14",
};

Map<String, dynamic> polaOlahragaFleksible4Minggu = {
  "1": "pola_olahraga_fleksibilitas_1",
  "2": "pola_olahraga_fleksibilitas_2",
  "3": "pola_olahraga_fleksibilitas_3",
  "4": "pola_olahraga_fleksibilitas_4",
  "5": "pola_olahraga_fleksibilitas_5",
  "6": "pola_olahraga_fleksibilitas_6",
  "7": "pola_olahraga_fleksibilitas_7",
  "8": "pola_olahraga_fleksibilitas_8",
  "9": "pola_olahraga_fleksibilitas_9",
  "10": "pola_olahraga_fleksibilitas_10",
  "11": "pola_olahraga_fleksibilitas_11",
  "12": "pola_olahraga_fleksibilitas_12",
  "13": "pola_olahraga_fleksibilitas_13",
  "14": "pola_olahraga_fleksibilitas_14",
  "15": "pola_olahraga_fleksibilitas_15",
  "16": "pola_olahraga_fleksibilitas_16",
  "17": "pola_olahraga_fleksibilitas_17",
  "18": "pola_olahraga_fleksibilitas_18",
  "19": "pola_olahraga_fleksibilitas_19",
  "20": "pola_olahraga_fleksibilitas_20",
  "21": "pola_olahraga_fleksibilitas_21",
  "22": "pola_olahraga_fleksibilitas_22",
  "23": "pola_olahraga_fleksibilitas_23",
  "24": "pola_olahraga_fleksibilitas_24",
  "25": "pola_olahraga_fleksibilitas_25",
  "26": "pola_olahraga_fleksibilitas_26",
  "27": "pola_olahraga_fleksibilitas_27",
  "28": "pola_olahraga_fleksibilitas_28",
  "29": "pola_olahraga_fleksibilitas_29",
  "30": "pola_olahraga_fleksibilitas_30",
};

Map<String, dynamic> polaOlahragaSenam2Minggu = {
  "1": "pola_olahraga_senam_1",
  "2": "pola_olahraga_senam_2",
  "3": "pola_olahraga_senam_3",
  "4": "pola_olahraga_senam_4",
  "5": "pola_olahraga_senam_5",
  "6": "pola_olahraga_senam_6",
  "7": "pola_olahraga_senam_7",
  "8": "pola_olahraga_senam_8",
  "9": "pola_olahraga_senam_9",
  "10": "pola_olahraga_senam_10",
  "11": "pola_olahraga_senam_11",
  "12": "pola_olahraga_senam_12",
  "13": "pola_olahraga_senam_13",
  "14": "pola_olahraga_senam_14",
};

Map<String, dynamic> polaOlahragaSenam4Minggu = {
  "1": "pola_olahraga_senam_1",
  "2": "pola_olahraga_senam_2",
  "3": "pola_olahraga_senam_3",
  "4": "pola_olahraga_senam_4",
  "5": "pola_olahraga_senam_5",
  "6": "pola_olahraga_senam_6",
  "7": "pola_olahraga_senam_7",
  "8": "pola_olahraga_senam_8",
  "9": "pola_olahraga_senam_9",
  "10": "pola_olahraga_senam_10",
  "11": "pola_olahraga_senam_11",
  "12": "pola_olahraga_senam_12",
  "13": "pola_olahraga_senam_13",
  "14": "pola_olahraga_senam_14",
  "15": "pola_olahraga_senam_15",
  "16": "pola_olahraga_senam_16",
  "17": "pola_olahraga_senam_17",
  "18": "pola_olahraga_senam_18",
  "19": "pola_olahraga_senam_19",
  "20": "pola_olahraga_senam_20",
  "21": "pola_olahraga_senam_21",
  "22": "pola_olahraga_senam_22",
  "23": "pola_olahraga_senam_23",
  "24": "pola_olahraga_senam_24",
  "25": "pola_olahraga_senam_25",
  "26": "pola_olahraga_senam_26",
  "27": "pola_olahraga_senam_27",
  "28": "pola_olahraga_senam_28",
  "29": "pola_olahraga_senam_29",
  "30": "pola_olahraga_senam_30",
};

Map<String, dynamic> polaOlahragaKardiovaskular2Minggu = {
  "1": "pola_olahraga_kardiovaskular_1",
  "2": "pola_olahraga_kardiovaskular_2",
  "3": "pola_olahraga_kardiovaskular_3",
  "4": "pola_olahraga_kardiovaskular_4",
  "5": "pola_olahraga_kardiovaskular_5",
  "6": "pola_olahraga_kardiovaskular_6",
  "7": "pola_olahraga_kardiovaskular_7",
  "8": "pola_olahraga_kardiovaskular_8",
  "9": "pola_olahraga_kardiovaskular_9",
  "10": "pola_olahraga_kardiovaskular_10",
  "11": "pola_olahraga_kardiovaskular_11",
  "12": "pola_olahraga_kardiovaskular_12",
  "13": "pola_olahraga_kardiovaskular_13",
  "14": "pola_olahraga_kardiovaskular_14",
};

Map<String, dynamic> polaOlahragaKardiovaskular4Minggu = {
  "1": "pola_olahraga_kardiovaskular_1",
  "2": "pola_olahraga_kardiovaskular_2",
  "3": "pola_olahraga_kardiovaskular_3",
  "4": "pola_olahraga_kardiovaskular_4",
  "5": "pola_olahraga_kardiovaskular_5",
  "6": "pola_olahraga_kardiovaskular_6",
  "7": "pola_olahraga_kardiovaskular_7",
  "8": "pola_olahraga_kardiovaskular_8",
  "9": "pola_olahraga_kardiovaskular_9",
  "10": "pola_olahraga_kardiovaskular_10",
  "11": "pola_olahraga_kardiovaskular_11",
  "12": "pola_olahraga_kardiovaskular_12",
  "13": "pola_olahraga_kardiovaskular_13",
  "14": "pola_olahraga_kardiovaskular_14",
  "15": "pola_olahraga_kardiovaskular_15",
  "16": "pola_olahraga_kardiovaskular_16",
  "17": "pola_olahraga_kardiovaskular_17",
  "18": "pola_olahraga_kardiovaskular_18",
  "19": "pola_olahraga_kardiovaskular_19",
  "20": "pola_olahraga_kardiovaskular_20",
  "21": "pola_olahraga_kardiovaskular_21",
  "22": "pola_olahraga_kardiovaskular_22",
  "23": "pola_olahraga_kardiovaskular_23",
  "24": "pola_olahraga_kardiovaskular_24",
  "25": "pola_olahraga_kardiovaskular_25",
  "26": "pola_olahraga_kardiovaskular_26",
  "27": "pola_olahraga_kardiovaskular_27",
  "28": "pola_olahraga_kardiovaskular_28",
  "29": "pola_olahraga_kardiovaskular_29",
  "30": "pola_olahraga_kardiovaskular_30",
};