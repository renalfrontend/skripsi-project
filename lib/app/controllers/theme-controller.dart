import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ThemeController extends GetxController{
  final RxInt countdown = 3.obs;
  final storage = GetStorage();
  late RxBool isDarkMode = false.obs;
  late RxBool IsLanguage = true.obs;
  late RxString language = "".obs;

  ThemeData get theme => isDarkMode.value ? AppTheme.darkTheme : AppTheme.lightTheme;

  void changeTheme(bool value) {
    storage.write("theme", value);
    isDarkMode.value = (storage.read("theme"))!;
  }

  void changeLanguage(bool value) async {
    storage.write("isLanguage", value);

    while (countdown.value > 0) {
      IsLanguage.value = storage.read("isLanguage")!;
      await Future.delayed(Duration(seconds: 1));
      if(countdown.value == 2) {
        Get.updateLocale(Locale(IsLanguage.value ? "id" : 'en'));
      }
      countdown.value--;
    }

    if(countdown.value == 0) {
      countdown.value = 3;
    }
  }

  @override
  void onInit() async {
    IsLanguage.value = storage.read('isLanguage')! == null ? true : storage.read('isLanguage')!;
    isDarkMode.value = storage.read('theme')! == null ? false : storage.read('theme')!;
    super.onInit();
  }
}