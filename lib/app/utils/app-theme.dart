import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class AppTheme {
  static Color lightBackgroundColor = Colors.white;
  static Color darkBackgroundColor = HexColor("#13193f");
  static Color primaryColorDark = HexColor("#8471DF");
  static Color splashScreenColor = HexColor("#4D35C5");
  static Color errorColor = HexColor(	"#ff3333");
  static Color backgroundColor = HexColor("#F7F7F5");

  static ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: primaryColorDark,
    scaffoldBackgroundColor: lightBackgroundColor,
    appBarTheme: AppBarTheme(
      backgroundColor: lightBackgroundColor,
      // color: Colors.black
    )
  );

  static ThemeData darkTheme = ThemeData.dark().copyWith(
    primaryColor: primaryColorDark,
    scaffoldBackgroundColor: darkBackgroundColor,
    appBarTheme: AppBarTheme(
      backgroundColor: darkBackgroundColor,
      
    ),
  );
}