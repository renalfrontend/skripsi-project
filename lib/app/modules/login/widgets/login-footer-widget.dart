import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/auth_controller.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class LoginFooterWidget extends StatelessWidget {
  const LoginFooterWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final authController = Get.find<AuthController>();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("login_or".tr),
        SizedBox(
          height: 20,
        ),
        SizedBox(
          width: double.infinity,
          height: 50,
          child: Obx(() => OutlinedButton.icon(
            icon: Image(
              image: AssetImage("assets/images/google.png"),
              width: 20,
            ),
            onPressed: () {
              authController.loginWithGoogle();
            },
            label: Obx(() => Text(
                  "login_provider".tr,
                  style: TextStyle(
                    color: themeController.isDarkMode.value
                        ? Colors.white
                        : Colors.black,
                  ),
                )),
            style: OutlinedButton.styleFrom(
                side: BorderSide(
                    color: themeController.isDarkMode.value
                        ? Colors.white
                        : Colors.black),
                foregroundColor: AppTheme.primaryColorDark),
          ),
        )),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("login_footer_desc".tr),
            TextButton(
              onPressed: () => Get.toNamed(Routes.SIGNUP),
              child: Text("login_signup".tr,
                  style: TextStyle(color: AppTheme.primaryColorDark)),
              style: TextButton.styleFrom(
                  foregroundColor: AppTheme.primaryColorDark),
            )
          ],
        )
      ],
    );
  }
}
