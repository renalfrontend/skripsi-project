import 'dart:convert';

import 'package:get/get.dart';
import 'package:skripsi_project/app/services/http-crypto.dart';

class Providers extends GetConnect{
  final url = "https://development.inhealth.co.id/ft-svc/api";

  Future<String> getProviders({
    String? cardNumber= "1001674112962",
    List<double>? coordinate,
    String? insuranceTpCd = "INSURANCE_TP_01",
    String? is_telemedika = "",
    String? limit = "10",
    String? nextPageUrl = "",
    String? order = "",
    String? partner_name = "",
    String? prevPageUrl = "",
    String? search = "",
    String? sort = "",
    String? username = "6289607266532",
  }) async {
    try {
      final bodyEncrypt = encryptBodyProvider(
        {
          "cardNumber" : cardNumber,
          "coordinate": coordinate,
          "insuranceTpCd": insuranceTpCd,
          "is_telemedika": is_telemedika,
          "limit": limit,
          "nextPageUrl": nextPageUrl,
          "order": order,
          "partner_name": partner_name,
          "prevPageUrl": prevPageUrl,
          "search": search,
          "sort": sort,
          "username": username
        }
      );

      final response = await post('$url/NWFaskesList_FH', bodyEncrypt).timeout(Duration(seconds: 30000));
      final data = json.encode(json.decode(response.body));
      final metadata = json.encode(json.decode(response.body)['metadata']);
      final code = int.parse(json.decode(metadata)['code']);

      if(code >= 200 && code < 300){
        return data;
      }else {
        throw(data);
      }
    } catch (e, s) {
      print("Error ${e}   STACKRACE $s");
      rethrow;
    }
  }

  Future<String> getProvidersDetail({
    List<double>? coordinate,
    String? cardNumber= "1001674112962",
    String? username = "6289607266532",
    String? providerId = "287"
  }) async {
    try {
      final bodyEncrypt = encryptBodyProvider({
        "cardNumber": cardNumber,
        "username": username,
        "coordinate": coordinate,
        "providerId": providerId
      });

      final response = await post('$url/NWFaskesDetail_FH', bodyEncrypt).timeout(Duration(seconds: 30000));
      final data = json.encode(json.decode(response.body));
      final metadata = json.encode(json.decode(response.body)['metadata']);
      final code = int.parse(json.decode(metadata)['code']);

      if(code >= 200 && code < 300){
        return data;
      }else {
        throw(data);
      }
    } catch (e, s) {
      rethrow;
    }
  }

}