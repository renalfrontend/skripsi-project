import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ProfileMenuWidget extends StatelessWidget {
  ProfileMenuWidget({
    super.key,
    required this.title,
    required this.icon,
    this.endIconSwitch = true,
    this.activeColorSwitch,
    this.inActiveColorSwitch,
    this.activeImageSwitch = "",
    this.inActiveImageSwitch = "",
    this.onChangeSwitch,
    this.switchValue,
    this.onTap
  });

  final String title, activeImageSwitch, inActiveImageSwitch;
  final IconData icon;
  final bool endIconSwitch;
  final RxBool? switchValue;
  final Color? activeColorSwitch, inActiveColorSwitch;  
  final Function(bool value)? onChangeSwitch;
  final Function()? onTap; 

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final iconColor = themeController.isDarkMode.value ? AppTheme.primaryColorDark : AppTheme.primaryColorDark;

    return Theme(
      data: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      child: GestureDetector(
        onTap: onTap,
        child: ListTile(
          splashColor: Colors.transparent,      
          visualDensity: VisualDensity.comfortable,
          style: ListTileStyle.drawer,      
          leading: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: iconColor.withOpacity(0.1)
            ),
            child: Icon(icon, color: iconColor),
          ),
          title: Obx(() => Text(title, style: TextStyle(color: themeController.isDarkMode.value ? Colors.white : Colors.black, fontSize: 16))),
          trailing: endIconSwitch ? Obx(() => Switch(
            value: switchValue?.value ?? themeController.isDarkMode.value,
            activeColor: activeColorSwitch,
            activeTrackColor: activeColorSwitch?.withOpacity(0.5),
            inactiveTrackColor: inActiveColorSwitch == Colors.white ? Colors.grey : inActiveColorSwitch,
            activeThumbImage: activeImageSwitch != "" ? AssetImage(activeImageSwitch) : null,
            inactiveThumbImage: inActiveImageSwitch != "" ? AssetImage(inActiveImageSwitch) : null,
            onChanged: (bool value) => onChangeSwitch!(value) ?? false,
          )) : SizedBox(),
        ),
      ),
    );
  }
}