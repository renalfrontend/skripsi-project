import 'package:get/get.dart';

import '../controllers/search_provider_page_controller.dart';

class SearchProviderPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchProviderPageController>(
      () => SearchProviderPageController(),
    );
  }
}
