import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

class ProfileHeaderSkeleton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    final mediaQuery = MediaQuery.of(context).size.width;

    return Column(
      children: [
        Obx(() => Shimmer.fromColors(
              baseColor: themeController.isDarkMode.value
                  ? Colors.grey.shade600
                  : Colors.grey.shade200,
              highlightColor: themeController.isDarkMode.value
                  ? Colors.white60
                  : Colors.white,
              child: SizedBox(
                  width: mediaQuery * 0.32,
                  height: 120,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: Colors.red),
                  )),
            )),
        SizedBox(height: 10),
        Obx(() => Shimmer.fromColors(
            baseColor: themeController.isDarkMode.value
                ? Colors.grey.shade600
                : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value
                ? Colors.white60
                : Colors.white,
            child: Container(
              width: mediaQuery * 0.5,
              height: 8,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.red),
            ))),
        SizedBox(height: 10),
        Obx(() => Shimmer.fromColors(
            baseColor: themeController.isDarkMode.value
                ? Colors.grey.shade600
                : Colors.grey.shade200,
            highlightColor: themeController.isDarkMode.value
                ? Colors.white60
                : Colors.white,
            child: Container(
              width: mediaQuery * 0.3,
              height: 8,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100), color: Colors.red),
            ))),
        SizedBox(height: 20),
        SizedBox(
            width: mediaQuery * 0.5,
            child: Obx(() => Shimmer.fromColors(
                  baseColor: themeController.isDarkMode.value
                      ? Colors.grey.shade600
                      : Colors.grey.shade200,
                  highlightColor: themeController.isDarkMode.value
                      ? Colors.white60
                      : Colors.white,
                  child: ElevatedButton(
                    onPressed: () => null,
                    child: Text("Edit Profile"),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        side: BorderSide.none,
                        shape: StadiumBorder()),
                  ),
                )))
      ],
    );
  }
}
