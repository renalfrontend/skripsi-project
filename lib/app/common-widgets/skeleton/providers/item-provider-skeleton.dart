import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemProviderSkeleton extends StatelessWidget {
  const ItemProviderSkeleton({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final themeController = Get.find<ThemeController>();

    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(10),
      child: Container(
            width: size.width,
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Obx(() => Shimmer.fromColors(
                  baseColor: themeController.isDarkMode.value
                        ? Colors.grey.shade600
                        : Colors.grey.shade200,
                    highlightColor: themeController.isDarkMode.value
                        ? Colors.white60
                        : Colors.white,
                  child: Container(
                    width: 150,
                    height: 150,      
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                      color: Colors.white
                    ),
                  ),
                )),
                Expanded(                                
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,                                  
                      children: [
                        Obx(() => Shimmer.fromColors(
                          baseColor: themeController.isDarkMode.value
                                ? Colors.grey.shade600
                                : Colors.grey.shade200,
                            highlightColor: themeController.isDarkMode.value
                                ? Colors.white60
                                : Colors.white,
                            child: Container(
                              width: 150,
                              height: 10,
                              color: Colors.white,
                            )
                        )),
                        SizedBox(height: 12),
                        Obx(() => Shimmer.fromColors(
                          baseColor: themeController.isDarkMode.value
                                ? Colors.grey.shade600
                                : Colors.grey.shade200,
                            highlightColor: themeController.isDarkMode.value
                                ? Colors.white60
                                : Colors.white,
                            child: Column(
                              children: [
                                Container(
                                  width: 130,
                                  height: 5,
                                  color: Colors.white,
                                ),
                                SizedBox(height: 5),
                                Container(
                                  width: 130,
                                  height: 5,
                                  color: Colors.white,
                                ),
                                SizedBox(height: 5),
                                Container(
                                  width: 130,
                                  height: 5,
                                  color: Colors.white,
                                ),
                                SizedBox(height: 5),
                                Container(
                                  width: 130,
                                  height: 5,
                                  color: Colors.white,
                                ),
                              ],
                            )
                        )),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
    );
  }
}