import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

// ignore: must_be_immutable
class ItemProviderHealth extends StatelessWidget {
  ItemProviderHealth({
    required this.providerBanner,
    required this.providerName,
    required this.providerLocation,
    required this.providerId
  });

  String providerBanner;
  String providerName;
  String providerLocation;
  String providerId;

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(20),
      child: Ink(
        decoration: BoxDecoration(
          color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: InkWell(
          onTap: () {Get.toNamed(Routes.DETAIL_PROVIDER, arguments: providerId);},
          borderRadius: BorderRadius.circular(20),
          highlightColor: Colors.grey.shade900.withOpacity(0.1),
          child: Container(
            width: 170,
            height: 160,
            child: Column(
              children: [
                SizedBox(
                  width: 170,
                  height: 90,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                    child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl: providerBanner,
                            progressIndicatorBuilder: (context, url, downloadProgress) => Center(child: CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark)),                            
                          ),
                    ),
                  ),
                Container(
                  width: 170,
                  height: 60,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  margin: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          providerName,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: themeController.isDarkMode.value ? Colors.white : Colors.black
                          ),
                        ),
                      ),
                      SizedBox(height: 7),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.location_on, size: 20, color: Colors.grey[600],),
                          SizedBox(width: 5),
                          Flexible(
                            child: Text(
                              providerLocation,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 11,
                                color: Colors.grey[500]
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
