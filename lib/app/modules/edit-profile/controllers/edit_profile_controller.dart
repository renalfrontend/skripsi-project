import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class EditProfileController extends GetxController {
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  FirebaseStorage storageRef = FirebaseStorage.instance;
  RxDouble progressUpload = 0.0.obs;
  RxString imageUrl = ''.obs;
  late TextEditingController fullname;
  late TextEditingController email;
  late TextEditingController password;
  var storage = GetStorage();

  Stream<DocumentSnapshot<Map<String, dynamic>>> userDetailStream() {
    return firestore
      .collection("users")
      .doc(storage.read('id-user'))
      .snapshots();
  }

  void initState() async {
    var idUser = storage.read('id-user');
    var docSnapshot = await firestore.collection('users').doc(idUser).get();
    var data = docSnapshot.data()!;

    await storage.write("login-provider", data['providerLogin']);
    fullname.text = data['fullname'];
    email.text = data['email'];
    password.text = data['password'];
    imageUrl.value = data['photo'];
  }

  // Delete user
  void deleteUser() async {
    User? user = await FirebaseAuth.instance.currentUser;
    
    Get.defaultDialog(      
      title: "deleteAccountTitle".tr,
      middleText: "deleteAccountDesc".tr,
      textConfirm: "Yes",      
      textCancel: "No",
      confirmTextColor: Colors.white,
      cancelTextColor: AppTheme.primaryColorDark,
      buttonColor: AppTheme.primaryColorDark,
      onConfirm: () async {
        Get.back();
        Get.defaultDialog(
          title: "deleteAccountTitleSuccses".tr,
          middleText: "deleteAccountTitleDesc".tr
        );

        firestore.collection('users').doc(storage.read('login-provider') != "" ? user?.providerData[0].uid : user?.uid).delete();
        storage.remove("login-provider");
        await user?.delete();
        await FirebaseAuth.instance.signOut();
        
        Get.offAllNamed(Routes.LOGIN);
      }
    );
  }

  // Update Account
  void updateAccount(String fullname, String email, String password) async{
    try{
      User? user = await FirebaseAuth.instance.currentUser;
          
      firestore.collection('users')
        .doc(storage.read('login-provider') != "" ? user?.providerData[0].uid : user?.uid)
        .update({
            "fullname": fullname,
            "email": email,
            "password": password,
        }); 

      user?.updateEmail(email);
      user?.updatePassword(password);

      Get.defaultDialog(
        title: "Berhasil",
        middleText: "Telah berhasil mengubah data akun",
        confirmTextColor: Colors.white,
        buttonColor: AppTheme.primaryColorDark,
        onConfirm: () => {
          Get.back(),
          Get.back()
        },
        textConfirm: "Okay"
      );
    }catch (e) {
      Get.defaultDialog(
        title: "errorTextGlobal".tr,
        middleText: e.toString()
      );
    }
  }

  // Upload file image
  void uploadFile() async{
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File file = File(result.files.single.path!);
      String fileName = result.files.single.name;
      
      try{
        await storageRef
          .ref("$fileName")
          .putFile(file)
          .snapshotEvents
          .listen((taskSnapshot) async {
            switch(taskSnapshot.state){
              case TaskState.running:
                final progress = 100.0 * (taskSnapshot.bytesTransferred / taskSnapshot.totalBytes);                
                print("PROGRESSS $progress");
                progressUpload.value = double.parse(progress.toString());
                break;
              case TaskState.paused:
                print("Paused");
                break;
              case TaskState.success:
                print("Succsesfully upload image");
                imageUrl.value = await storageRef.ref("$fileName").getDownloadURL();
                await firestore.collection('users').doc(storage.read('id-user')).update({
                  'photo': imageUrl.value
                });
                break;
              case TaskState.canceled:
                print("Uploaded canceled");
                break;
              case TaskState.error:
                print("Error");
                break;
            }
          });
      }on FirebaseException catch (e) {
        print(e);
      }
    } else {
      print("Membatalkan file upload");
    }
  }

  @override
  void onInit() async {
    initState();

    fullname = TextEditingController(text: "");
    email = TextEditingController(text: "");
    password = TextEditingController(text: "");    

    super.onInit();
  }

  @override
  void onClose() {
    fullname.dispose();
    email.dispose();
    password.dispose();
    super.onClose();
  }
}
