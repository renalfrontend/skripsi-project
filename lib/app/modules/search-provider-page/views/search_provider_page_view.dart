import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/item-provider.dart';
import 'package:skripsi_project/app/common-widgets/skeleton/providers/item-provider-skeleton.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/search_provider_page_controller.dart';

class SearchProviderPageView extends GetView<SearchProviderPageController> {
  SearchProviderPageView({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,      
        title: Text("search_provider_title".tr, style: Theme.of(context).textTheme.titleLarge),
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.angle_left, color: themeController.isDarkMode.value ? Colors.white70 : Colors.black87,),
          onPressed: () => Get.back(),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: [
              Divider(
                height: 0.1,
                thickness: 0,
                color: themeController.isDarkMode.value ? Colors.grey[700] : Colors.grey[350]
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: Row(
                children: [
                  Icon(Icons.location_on, color: AppTheme.primaryColorDark),
                  SizedBox(width: 8),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("search_provider_location".tr, style: TextStyle(fontSize: 12),),
                      SizedBox(height: 5),
                      Obx(() => Text(controller.location.value, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 13)))
                    ],
                  ),
                ],
              ),
              ),
              Divider(
                height: 0.1,
                thickness: 0,
                color: themeController.isDarkMode.value ? Colors.grey[700] : Colors.grey[350]
              ),
              // Input Search
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                margin: EdgeInsets.symmetric(vertical: 15),
                child: Container(
                  width: size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  margin: EdgeInsets.all(0),
                  decoration: BoxDecoration(                  
                    borderRadius: BorderRadius.circular(20),
                    color: themeController.isDarkMode.value ? HexColor("#0e122f") : Colors.grey[100]
                  ),
                  child: TextField(
                    controller: controller.searchInput,
                    decoration: InputDecoration(                    
                      contentPadding: EdgeInsets.only(top: 15),
                      border: InputBorder.none,
                      hintText: "search_provider_placeholder".tr,
                      hintStyle: TextStyle(color: Colors.grey[400]),
                      suffixIcon: GestureDetector(
                        onTap: () => controller.getAllProviders(type: 'search', search: controller.searchInput.text),
                        child: Icon(Icons.search, color: Colors.grey[800]),
                      ),
                      suffixStyle: TextStyle(fontSize: 0)
                    ),
                    style: TextStyle(fontSize: 13),
                    cursorColor: AppTheme.primaryColorDark,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Obx(() => Text("${controller.totalProviders.value} ${'Search_provider_desc'.tr}", style: TextStyle(fontSize: 12),)),
              ),
              Expanded(
                child: Obx(() => Container(
                  color: themeController.isDarkMode.value ? HexColor("#0e122f") : AppTheme.backgroundColor,
                  child: controller.obx((state) => 
                    ListView.separated(
                      itemCount: (state!['data'] as List<dynamic>).length,
                      padding: EdgeInsets.all(20),
                      clipBehavior: Clip.hardEdge,
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 20);
                      },
                      itemBuilder: (context, index) {
                        final data = state['data'][index];

                        return ItemProvider(
                          size: size,
                          providerId: data['provider_id'].toString(),
                          imageurl: data['provider_banner'] != null ? data['provider_banner'] : "https://down-ph.img.susercontent.com/file/ceda67835f47cbee57d0d547297c3956_tn",
                          title: data['provider_name'] != null ? data['provider_name'] : "Tidak Tersedia",
                          status: data['open_today'] != null ? data['open_today'] : "-",
                          km: data['location']['distance'] != null ? data['location']['distance'] : "-",
                          location: data['location']['location_gmaps'] != null ? data['location']['location_gmaps'] : "-",
                        );
                      },
                    ),
                    onLoading: ListView.separated(
                      itemCount: 10,
                      padding: EdgeInsets.all(20),
                      clipBehavior: Clip.hardEdge,
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 20);
                      },
                      itemBuilder: (context, index) {
                        return ItemProviderSkeleton();
                      },
                    ),
                    onError: (error) => Container(                      
                      width: size.width,
                      child: Center(
                        child: Text("not_found_text".tr),
                      ),
                    ),
                  )                
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}