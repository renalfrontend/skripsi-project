import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

class ItemProvider extends StatelessWidget {
  const ItemProvider({
    super.key,
    required this.size,
    required this.imageurl,
    required this.title,
    required this.status,
    required this.km,
    required this.location,
    required this.providerId
  });

  final Size size;
  final String imageurl;
  final String title;
  final String status;
  final String km;
  final String location;
  final String providerId;

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();
    
    return Material(
      elevation: 10,
      borderRadius: BorderRadius.circular(10),
      child: Ink(
        decoration: BoxDecoration(
          color: themeController.isDarkMode.value ? AppTheme.darkBackgroundColor : Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: InkWell(
          onTap: () {Get.toNamed(Routes.DETAIL_PROVIDER, arguments: providerId);},
          borderRadius: BorderRadius.circular(10),
          child: Container(
            width: size.width,
            height: 150,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 150,
                  height: 150,                                
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                    child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: imageurl,
                        progressIndicatorBuilder: (context, url, downloadProgress) => Center(child: CircularProgressIndicator(value: downloadProgress.progress, color: AppTheme.primaryColorDark)),
                      ),
                  ),
                ),
                Expanded(                                
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,                                  
                      children: [
                        Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              color: status == "Open" ? AppTheme.primaryColorDark : Colors.red
                            )
                          ),
                          child: Text(status, textAlign: TextAlign.center, style: TextStyle(color: status == "Open" ? AppTheme.primaryColorDark : Colors.red),),
                        ),
                        SizedBox(height: 18),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(Icons.location_on, color: Colors.grey[700], size: 18,),
                            SizedBox(width: 5),
                            Expanded(child: Text("(${km.split('.')[0]}km) ${location}", style: TextStyle(fontSize: 12, color: Colors.grey[600]), overflow: TextOverflow.ellipsis,))
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
