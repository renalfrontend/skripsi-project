import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/routes/app_pages.dart';

import '../controllers/welcome_controller.dart';

class WelcomeView extends GetView<WelcomeController> {
  const WelcomeView({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      body: Obx(() => Container(
        color: themeController.isDarkMode.value ? HexColor("#13193F") : HexColor("#6C63FF"),
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SvgPicture.asset(
              themeController.isDarkMode.value ? "assets/images/welcome-darkmode.svg" : "assets/images/welcome-ligtmode.svg",
              height: mediaQuery.size.height * 0.5
            ),
            Column(
              children: [
                Text(
                  "welcome_title".tr,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.white
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "welcome_body".tr,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white70
                  ),
                )
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: OutlinedButton(
                    onPressed: () => Get.toNamed(Routes.LOGIN),
                    child: Text(
                      "welcome_button1".tr,
                      style: TextStyle(color: Colors.white),
                    ),
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(color: Colors.white),
                      foregroundColor: Colors.white60
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: ElevatedButton(
                    onPressed: () => Get.toNamed(Routes.SIGNUP),
                    child: Text(
                      "welcome_button2".tr,
                      style: TextStyle(
                        color: Colors.white,
                      )
                    ),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: themeController.isDarkMode.value ? HexColor("#8471DF") : HexColor("#8D87FF"),
                      foregroundColor: Colors.white10,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ))
    );
  }

}