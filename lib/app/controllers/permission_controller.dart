import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PermissionController extends GetxController {
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;
    
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return false;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {   
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return false;
    }

    return true;
  }

  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();

    if (!hasPermission) {
      AwesomeNotifications().isNotificationAllowed().then((isAllowed) => {
        if(!isAllowed){
          AwesomeNotifications().requestPermissionToSendNotifications()
        }
      });
      
      return;
    }

    await Geolocator
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
      .then((Position position) {
        GetStorage().write("latitude", position.latitude);
        GetStorage().write("longtitude", position.longitude);

        AwesomeNotifications().isNotificationAllowed().then((isAllowed) => {
          if(!isAllowed){
            AwesomeNotifications().requestPermissionToSendNotifications()
          }
        });
      })
    .catchError((e) {
      debugPrint(e);
    });
  }

  @override
  void onInit() async {
    await _getCurrentPosition();
    super.onInit();
  }
}
