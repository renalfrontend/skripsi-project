import 'package:flutter/material.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

// ignore: must_be_immutable
class LoadingScreen extends StatelessWidget {
  LoadingScreen({
    required this.bodyHeight
  });

  double bodyHeight;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: bodyHeight,
      color: Colors.black.withOpacity(0.7),
      child: Center(
        child: CircularProgressIndicator(
          color: AppTheme.primaryColorDark,
        ),
      ),
    );
  }
}