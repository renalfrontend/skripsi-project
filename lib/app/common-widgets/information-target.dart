import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';

class InformationTarget extends StatelessWidget {
  InformationTarget({
    super.key,
    required this.namaTarget,
    required this.namaPola,
    required this.waktuTarget,
    required this.text,
    required this.statusTarget,
    required this.createDateTarget,
    required this.endDateTarget,
  });

  String namaTarget;
  String namaPola;
  String waktuTarget;
  String text;
  String? statusTarget;
  DateTime createDateTarget;
  DateTime endDateTarget;

  @override
  Widget build(BuildContext context) {
    final themeController = Get.find<ThemeController>();

    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(bottom: 30, top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text("${"detail_target_title1".tr}: ".tr, style: TextStyle(fontWeight: FontWeight.bold)),
                Text(namaTarget.tr)
              ]
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Text("${"detail_target_title2".tr}: ".tr, style: TextStyle(fontWeight: FontWeight.bold)),
                Text(namaPola.tr)
              ]
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Text("${"detail_target_title3".tr}: ".tr, style: TextStyle(fontWeight: FontWeight.bold)),
                Text(waktuTarget.tr)
              ]
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Text("${"createTargetDateText".tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Row(
                  children: [
                    Text("${createDateTarget.day}-"),
                    Text("${createDateTarget.month}-"),
                    Text("${createDateTarget.year}")
                  ],
                )
              ]
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Text("${"endTargetDateText".tr}: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Row(
                  children: [
                    Text("${endDateTarget.day}-"),
                    Text("${endDateTarget.month}-"),
                    Text("${endDateTarget.year}")
                  ],
                )
              ]
            ),
            SizedBox(height: 10),
            Row(
              children: [
                Text("Progress: ", style: TextStyle(fontWeight: FontWeight.bold)),
                Text("$statusTarget".toString().tr, style: TextStyle(color: statusTarget.toString().tr == 'Sudah Selesai' || statusTarget.toString().tr == 'Target Completed' ? Colors.green : themeController.isDarkMode.value ? statusTarget.toString().tr == 'Sudah Selesai' || statusTarget.toString().tr == 'Target Completed' ? Colors.green : Colors.white : Colors.black),)
              ]
            ),
            SizedBox(height: 10),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${"detail_target_title4".tr} ${namaPola.tr}", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 5),
                  Text(text.tr,
                    textAlign: TextAlign.left,
                  )
                ],
              ),      
            ),
            
          ],
        ),
      ),
    );
  }
}