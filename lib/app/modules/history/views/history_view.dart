import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:skripsi_project/app/common-widgets/item-my-target.dart';
import 'package:skripsi_project/app/controllers/theme-controller.dart';
import 'package:skripsi_project/app/utils/app-theme.dart';

import '../controllers/history_controller.dart';

class HistoryView extends GetView<HistoryController> {
  const HistoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final themeController = Get.find<ThemeController>();

    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.transparent,      
        title: Text("menu_4".tr, style: Theme.of(context).textTheme.titleLarge),
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.angle_left, color: themeController.isDarkMode.value ? Colors.white70 : Colors.black87,),
          onPressed: () => Get.back(),
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Container(
          margin: EdgeInsets.only(top: 10),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 20, top: 10),
                child: Text("history_desc".tr, style: TextStyle(fontSize: 12),),
              ),
              Expanded(
                child: Obx(() => Container(
                  color: themeController.isDarkMode.value ? HexColor("#0e122f") : AppTheme.backgroundColor,
                  child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                    stream: controller.streamHistory(),
                    builder: (context, snapshot) {
                      final data = snapshot.data?.docs;

                      if(snapshot.connectionState == ConnectionState.active){
                        if(data?.length != 0){
                          return ListView.builder(
                            itemCount: data!.length,
                            padding: EdgeInsets.all(20).copyWith(top: 0),
                            itemBuilder: (context, index) {
                              var dataHistory = data[index].data();

                              return Dismissible(
                                key: Key(dataHistory['id']),
                                direction: DismissDirection.endToStart,
                                background: Container(
                                  alignment: Alignment.centerRight,
                                  padding: EdgeInsets.only(right: 10),
                                  child: Icon(Icons.delete),
                                  decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius: BorderRadius.all(Radius.circular(20)),
                                  ),
                                ),
                                confirmDismiss: (direction){
                                  return showDialog(
                                    context: context, 
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text("confirm_popup_title".tr),
                                        content: Text("history_popup_desc".tr),
                                        actions: [
                                          OutlinedButton(
                                            onPressed: () {
                                              Navigator.of(context).pop(false);
                                            },
                                            style: OutlinedButton.styleFrom(
                                              foregroundColor: AppTheme.primaryColorDark
                                            ),
                                            child: Text("confirm_popup_btn_no".tr, style: TextStyle(color: AppTheme.primaryColorDark),)                                      
                                          ),
                                          ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: AppTheme.primaryColorDark
                                            ),
                                            onPressed: () {
                                              controller.deleteHistory(dataHistory['id']);
                                              Navigator.of(context).pop(true);
                                            }, 
                                            child: Text("confirm_popup_btn_yes".tr)
                                          )
                                        ],
                                      );
                                    }
                                  );
                                },
                                child: GestureDetector(
                                  child: ItemMyTarget(
                                    id: dataHistory['id'],
                                    titleTarget: dataHistory['targetName'],
                                    desc1: dataHistory['targetPolaDetail'],
                                    desc2: dataHistory['targetWaktu'],
                                    color: dataHistory['colorTarget'],
                                    type: "history",
                                  )
                                )
                              );
                            },
                          );                                
                        }else {
                          return Center(
                            child: Text('history_empty'.tr),
                          );
                        }
                      }

                      return SizedBox(
                        width: 50,
                        height: 50,
                        child: CircularProgressIndicator(color: AppTheme.primaryColorDark),
                      );
                    },
                  )
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
